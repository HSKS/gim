# GIM SaaS服务

GIM 只是一款简单的单机版的IM SaaS服务。附上demo视频链接：[demo视频](demo.mp4)

GIM旨在助力小型创业公司，在资金紧缺，人手不足的情况下如何在更短的时间内在自己的app中加入IM、音视频功能；短平快的实现业务的成功。

当然了如果GIM无法满足您目前的业务，需要实现每日亿级，分布式高并发的IM服务，可以加我QQ（891806797）一起沟通交流

亿级用户分布式IM服务需要实现以下几个子系统:

1、IM业务系统

2、信令系统

3、长连接系统

4、推送系统

5、存储系统

并且需要用到的中间件也不是GIM可比的，需要用到Mysql，Redis，Kafka/pulsarMQ，Hbase/TiDB/Clickhouse，canal等等
并且实现复杂度也不是GIM可比的，需要关注数据的冷热分离，消息的即时触达，数据一致性，以及中间价之间的协调等问题


## 环境
Golang 1.18.3

Flutter 2.8.1

## gim 提供了以下的功能：

### 1、单聊消息：
1）消息发送

2）消息接收

3）消息撤回

4）消息已读未读提醒

5）未读消息数量变更

6）标记消息已读未读

7）获取消息列表

8）消息推送（腾讯云TPNS）

9）内容安全审核（腾讯云COS）

### 会话
1）获取会话列表

2）置顶会话

3）删除会话

### 通用API
1）获取COS上传put url

2）修改应用是否出于前台（如果应用处于后台发送消息会提醒推送，否则静默推送）

### 开放API
1）SaaS服务主要考虑三方应用直接对接，因此提供了获取登录GIMToken的api供三方服务获取接入Token

### 音视频通话
1）腾讯云TRTC 获取接入Token的api

2）声网ARTC 获取接入Token的api

### 信令
1）发起信令

2）接受邀请

3）拒绝邀请

4）取消邀请

5）获取最后一次信令（假如处于后台，为了保障点击通知打开APP，通知服务端下发最后一次有效信令，呼起电话接听holding页面）

## 注意事项

您的app接入GIM前一定要登录腾讯云创建一个桶，并且开通`内容审核`功能，视频目录（gim/video/）配置`智能封面工作流`，在表tb_application里配置应用信息：

```cgo
create table if not exists tb_application
(
    id                      int         not null auto_increment unique,
    app_id                  varchar(30) not null comment '',
    cb_url                  varchar(255) default '' comment '',
    is_prod                 boolean      default true comment '',
    token                   varchar(150) not null  comment '请求验证密钥',
    tpns_android_access_id  int          default 0 comment '',
    tpns_android_secret_key varchar(100) default '' comment '',
    tpns_ios_access_id      int          default 0 comment '',
    tpns_ios_secret_key     varchar(100) default '' comment '',
    primary key (id)
) engine = InnoDB
  auto_increment = 1000001
  default charset = utf8mb4
  collate = utf8mb4_0900_ai_ci comment '应用表';
```

为了防止多个app同时接入gim，导致用户重叠，因此gim内部用户推荐使用（`appId_外部数据库ID` 比如：`gim_12345`）的格式来接入。

gim不存储用户信息，只提供简单的im能力，关系链请您自己的服务来维护

`tb_user_demo`这个表没有实际用处，只是为了方便写demo用的，实际有用的表只有前面几个表，api下的`demo`目录也是方便example调用写的demo，没实际用处

为了方便您的对接，我已经写好Flutter接入plugin，并且example写了一个demo，接入了GIM的核心功能，以及使用被大家广泛使用的声网实现了音视频通话功能

## 当然了俺的项目帮到了您，俺不需要您帮俺买杯奶茶，俺只想要一个大大的👍

如果各位有发现啥问题，可以给俺发邮件哈（891806797@qq.com）