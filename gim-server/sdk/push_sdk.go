package sdk

import (
	"context"
	"gim-server/sdk/tpns"
)

type PushOneAccountArgs struct {
	PushType        string
	IsProd          bool
	AccessId        uint32
	SecretKey       string
	FirebaseAuthKey string
	Notify          bool
	Account         string
	DeviceToken     []string
	Title           string
	Content         string
	BadgeType       int
}

func PushOneAccount(args *PushOneAccountArgs) error {
	if args.PushType == "firebase" {
		if len(args.DeviceToken) == 1 && args.Notify {
			return PushOneDevice(context.Background(), args.FirebaseAuthKey, args.DeviceToken[0], args.Title, args.Content)
		}
		return nil
	} else {
		if args.AccessId == 0 || args.SecretKey == "" {
			return nil
		}
		req := tpns.NewRequest(
			tpns.WithAudience(tpns.AudienceAccount),
			tpns.WithAccountList([]string{args.Account}),
			tpns.WithMessageType(func() tpns.MessageType {
				if args.Notify {
					return tpns.Notify
				} else {
					return tpns.Message
				}
			}()),
			tpns.WithTitle(args.Title),
			tpns.WithContent(args.Content),
			tpns.WithAndroidMessage(&tpns.AndroidMessage{
				BadgeType: args.BadgeType,
			}),
			tpns.WithIOSMessage(&tpns.IOSMessage{
				Aps: &tpns.IOSAps{
					BadgeType: args.BadgeType,
				},
			}),
			tpns.WithEnvironment(func() tpns.EnvironmentType {
				if !args.IsProd {
					return tpns.Develop
				} else {
					return tpns.Product
				}
			}()),
		)
		_, err := tpns.NewClient(tpns.ShanghaiHost, args.AccessId, args.SecretKey).Do(req)
		return err
	}
}
