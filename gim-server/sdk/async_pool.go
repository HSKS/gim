package sdk

import (
	"github.com/panjf2000/ants/v2"
	"runtime"
	"sync"
)

var (
	asyncPool     *ants.Pool
	asyncPoolOnce sync.Once
)

func AsyncPool() *ants.Pool {
	asyncPoolOnce.Do(func() {
		var err error
		asyncPool, err = ants.NewPool(runtime.NumCPU() * 1000)
		if err != nil {
			panic(err)
		}
	})
	return asyncPool
}
