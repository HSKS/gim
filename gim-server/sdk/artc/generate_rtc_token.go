package artc

import (
	"gim-server/config"
	rtctokenbuilder "github.com/AgoraIO/Tools/DynamicKey/AgoraDynamicKey/go/src/RtcTokenBuilder"
	"time"
)

func GenerateStringUidRtcToken(userAccount string, channelName string, role rtctokenbuilder.Role) (string, error) {
	cfg := config.GetCfg()
	// Token 过期的时间，单位为秒
	// 为作演示，在此将过期的时间戳设为 40 秒。当 Token 即将过期时，你可以看到客户端自动更新 Token 的过程
	expireTimeInSeconds := uint32(180)
	// 获取当前时间戳
	currentTimestamp := uint32(time.Now().UTC().Unix())
	// Token 过期的 Unix 时间戳
	expireTimestamp := currentTimestamp + expireTimeInSeconds

	return rtctokenbuilder.BuildTokenWithUserAccount(cfg.ArtcAppID, cfg.ArtcCertificate, channelName, userAccount, role, expireTimestamp)
}

func GenerateIntUidRtcToken(uid uint32, channelName string, role rtctokenbuilder.Role) (string, error) {
	cfg := config.GetCfg()
	// Token 过期的时间，单位为秒
	// 为作演示，在此将过期的时间戳设为 40 秒。当 Token 即将过期时，你可以看到客户端自动更新 Token 的过程
	expireTimeInSeconds := uint32(180)
	// 获取当前时间戳
	currentTimestamp := uint32(time.Now().UTC().Unix())
	// Token 过期的 Unix 时间戳
	expireTimestamp := currentTimestamp + expireTimeInSeconds

	return rtctokenbuilder.BuildTokenWithUID(cfg.ArtcAppID, cfg.ArtcCertificate, channelName, uid, role, expireTimestamp)
}
