package model

type TbSignalingInviteMsg struct {
	ID        int64  `json:"id"`
	FromUID   string `json:"from_uid"`
	ToUID     string `json:"to_uid"`
	Action    int    `json:"action"` //0待接收 1已接收 2已取消 3已拒绝
	Expire    int64  `json:"expire"`
	CreatedAt int64  `json:"created_at"`
}

func (t *TbSignalingInviteMsg) TableName() string {
	return "tb_signaling_invite_msg"
}
