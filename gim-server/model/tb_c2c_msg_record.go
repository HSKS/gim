package model

const (
	SucCode       = 0
	AuditCode     = 1
	BlacklistCode = 2
	CbInterceptor = 3
)

const (
	MsgTypeText      = "text"
	MsgTypeImage     = "image"
	MsgTypeVideo     = "video"
	MsgTypeAudio     = "audio"
	MsgTypeGeo       = "geo"
	MsgTypeSignaling = "signaling"
	MsgTypeCustom    = "custom"
)

type TbC2cMsgRecord struct {
	ID        int64  `json:"id"`
	PreID     string `json:"pre_id" gorm:"-"`
	FromUID   string `json:"from_uid"`
	ToUID     string `json:"to_uid"`
	MsgType   string `json:"msg_type"`
	MsgData   string `json:"msg_data"`
	Code      int    `json:"code"`
	Ext       string `json:"ext"`
	Readed    bool   `json:"readed"`
	ErrTip    string `json:"err_tip"`
	DelUID    string `json:"del_uid"` //"0"未删除 "-1"全删除 用户ID 单方删除
	CreatedAt int64  `json:"created_at"`
}

func (t *TbC2cMsgRecord) TableName() string {
	return "tb_c2c_msg_record"
}
