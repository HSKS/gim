package model

type TbApplication struct {
	ID                   int64  `json:"id"`
	AppID                string `json:"app_id"`
	CbURL                string `json:"cb_url"`
	IsProd               bool   `json:"is_prod"`
	Token                string `json:"token"`
	IgnoreAudit          bool   `json:"ignore_audit"`
	PushType             string `json:"push_type"`
	TpnsAndroidAccessId  uint32 `json:"tpns_android_access_id"`
	TpnsAndroidSecretKey string `json:"tpns_android_secret_key"`
	TpnsIosAccessId      uint32 `json:"tpns_ios_access_id"`
	TpnsIosSecretKey     string `json:"tpns_ios_secret_key"`
	FirebaseAuthKey      string `json:"firebase_auth_key"`
	CreatedAt            int64  `json:"created_at"`
}

func (t *TbApplication) TableName() string {
	return "tb_application"
}
