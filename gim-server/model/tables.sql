create table if not exists tb_user_demo
(
    id          int         not null auto_increment unique,
    uid         varchar(50) not null comment '',
    nickname    varchar(30)  default '' comment '',
    avatar      varchar(255) default '' comment '',
    gender      tinyint      default 0 comment '',
    reg_step    tinyint      default 0 comment '',
    created_at  int          default 0 comment '',
    last_active int          default 0 comment '',
    primary key (id),
    key         tb_user_demo_uid(uid) using btree,
    key         tb_user_demo_last_active(last_active) using btree
) engine = InnoDB
  auto_increment = 1000001
  default charset = utf8mb4
  collate = utf8mb4_0900_ai_ci
comment
'用户测试表';

create table if not exists tb_application
(
    id                      int          not null auto_increment unique,
    app_id                  varchar(30)  not null comment '',
    cb_url                  varchar(255) default '' comment '',
    is_prod                 boolean      default true comment '',
    token                   varchar(150) not null comment '请求验证密钥',
    ignore_audit            boolean      default false comment '',
    tpns_android_access_id  int          default 0 comment '',
    tpns_android_secret_key varchar(100) default '' comment '',
    tpns_ios_access_id      int          default 0 comment '',
    tpns_ios_secret_key     varchar(100) default '' comment '',
    firebase_auth_key       varchar(255) default '' comment '',
    created_at              int          default 0 comment '',
    primary key (id)
) engine = InnoDB
  auto_increment = 1000001
  default charset = utf8mb4
  collate = utf8mb4_0900_ai_ci
comment
'应用表';

create table if not exists tb_c2c_msg_record
(
    id         int         not null auto_increment unique,
    from_uid   varchar(50)   default '' comment '',
    to_uid     varchar(50)   default '' comment '',
    msg_type   varchar(30) not null comment '',
    msg_data   varchar(4096) default '' comment '',
    code       tinyint       default 0 comment '',
    readed     boolean       default false comment '',
    err_tip    varchar(100)  default '' comment '',
    del_uid    varchar(50)   default '0' comment '',
    created_at int           default 0 comment '',
    primary key (id),
    key        tb_c2c_msg_record_from_uid_to_uid(from_uid, to_uid) using btree,
    key        tb_c2c_msg_record_to_uid_from_uid(to_uid, from_uid) using btree,
    key        tb_c2c_msg_record_created_at(created_at) using btree
) engine = InnoDB
  auto_increment = 1000001
  default charset = utf8mb4
  collate = utf8mb4_0900_ai_ci
comment
'IM消息表';

create table if not exists tb_conversation
(
    id           int not null auto_increment unique,
    from_uid     varchar(50) default '' comment '',
    to_uid       varchar(50) default '' comment '',
    unread_count int         default 0 comment '',
    mark_top     boolean     default false comment '',
    created_at   int         default 0 comment '',
    updated_at   int         default 0 comment '',
    primary key (id),
    key          tb_conversation_from_uid_to_uid(from_uid, to_uid) using btree,
    key          tb_conversation_updated_at(updated_at) using btree
) engine = InnoDB
  auto_increment = 1000001
  default charset = utf8mb4
  collate = utf8mb4_0900_ai_ci
comment
'会话列表';

create table if not exists tb_signaling_invite_msg
(
    id         int not null auto_increment unique,
    from_uid   varchar(50) default '' comment '',
    to_uid     varchar(50) default '' comment '',
    action     int         default 0 comment '',
    expire     int         default 0 comment '',
    created_at int         default 0 comment '',
    primary key (id),
    key        tb_signaling_invite_msg_to_uid(to_uid) using btree,
    key        tb_signaling_invite_msg_expire(expire) using btree
) engine = InnoDB
  auto_increment = 1000001
  default charset = utf8mb4
  collate = utf8mb4_0900_ai_ci
comment
'信令邀请记录表，临时存储';