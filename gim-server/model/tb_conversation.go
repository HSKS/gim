package model

type TbConversation struct {
	ID          int64           `json:"id"`
	FromUID     string          `json:"from_uid"`
	ToUID       string          `json:"to_uid"`
	UnreadCount int64           `json:"unread_count"`
	MarkTop     bool            `json:"mark_top"`
	Ext         string          `json:"ext"`
	CreatedAt   int64           `json:"created_at"`
	UpdatedAt   int64           `json:"updated_at"`
	LastMsg     *TbC2cMsgRecord `json:"last_msg" gorm:"-"`
}

func (t *TbConversation) TableName() string {
	return "tb_conversation"
}
