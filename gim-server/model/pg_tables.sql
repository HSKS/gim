create table if not exists tb_user_demo
(
    id          serial8 primary key,
    uid         varchar(50)  not null,
    nickname    varchar(30)  not null default '',
    avatar      varchar(255) not null default '',
    gender      int2         not null default 0,
    reg_step    int2         not null default 0,
    created_at  int8         not null default 0,
    last_active int8         not null default 0,
    CONSTRAINT tb_user_demo PRIMARY KEY (id)
);

comment on table tb_user_demo is '测试用户表';

CREATE INDEX tb_user_demo_uid ON tb_user_demo (uid);
CREATE INDEX tb_user_demo_last_active ON tb_user_demo (last_active);


create table if not exists tb_application
(
    id                      serial8 primary key,
    app_id                  varchar(30)  not null,
    cb_url                  varchar(255) not null default '',
    is_prod                 boolean      not null default true,
    token                   varchar(150) not null,
    ignore_audit            boolean      not null default false,
    push_type               varchar(30)  not null default 'firebase',
    tpns_android_access_id  int8         not null default 0,
    tpns_android_secret_key varchar(100) not null default '',
    tpns_ios_access_id      int8         not null default 0,
    tpns_ios_secret_key     varchar(100) not null default '',
    firebase_auth_key       varchar(255) not null default '',
    created_at              int8         not null default 0,
    CONSTRAINT tb_application PRIMARY KEY (id)
);

comment on table tb_application is '应用配置表';

create table if not exists tb_c2c_msg_record
(
    id         serial8 primary key,
    from_uid   varchar(50)   not null default '',
    to_uid     varchar(50)   not null default '',
    msg_type   varchar(30)   not null,
    msg_data   varchar(4096) not null default '',
    code       int2          not null default 0,
    ext        text          not null default '',
    readed     boolean       not null default false,
    err_tip    varchar(100)  not null default '',
    del_uid    varchar(50)   not null default '0',
    created_at int8          not null default 0,
    CONSTRAINT tb_c2c_msg_record PRIMARY KEY (id)
);
comment on table tb_c2c_msg_record is 'IM消息表';
CREATE INDEX tb_c2c_msg_record_from_uid_to_uid ON tb_c2c_msg_record (from_uid, to_uid);
CREATE INDEX tb_c2c_msg_record_to_uid_from_uid ON tb_c2c_msg_record (to_uid, from_uid);
CREATE INDEX tb_c2c_msg_record_created_at ON tb_c2c_msg_record (created_at);


create table if not exists tb_conversation
(
    id           serial8 primary key,
    from_uid     varchar(50) not null default '',
    to_uid       varchar(50) not null default '',
    unread_count int4        not null default 0,
    mark_top     boolean     not null default false,
    ext          text        not null default '',
    created_at   int8        not null default 0,
    updated_at   int8        not null default 0,
    CONSTRAINT tb_conversation PRIMARY KEY (id)
);
comment on table tb_conversation is '会话列表';
CREATE INDEX tb_conversation_from_uid_to_uid ON tb_conversation (from_uid, to_uid);
CREATE INDEX tb_conversation_updated_at ON tb_conversation (updated_at);

create table if not exists tb_signaling_invite_msg
(
    id         serial8 primary key,
    from_uid   varchar(50) not null default '',
    to_uid     varchar(50) not null default '',
    action     int2        not null default 0,
    expire     int8        not null default 0,
    created_at int8        not null default 0,
    CONSTRAINT tb_signaling_invite_msg PRIMARY KEY (id)
);
comment on table tb_signaling_invite_msg is '信令邀请记录表，临时存储';
CREATE INDEX tb_signaling_invite_msg_to_uid ON tb_signaling_invite_msg (to_uid);
CREATE INDEX tb_signaling_invite_msg_expire ON tb_signaling_invite_msg (expire);