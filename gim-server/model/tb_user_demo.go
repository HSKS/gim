package model

type TbUserDemo struct {
	ID         int64  `json:"id"`
	UID        string `json:"uid"`
	Nickname   string `json:"nickname"`
	Avatar     string `json:"avatar"`
	Gender     int    `json:"gender"`
	RegStep    int    `json:"reg_step"`
	CreatedAt  int64  `json:"created_at"`
	LastActive int64  `json:"last_active"`
}

func (t *TbUserDemo) TableName() string {
	return "tb_user_demo"
}
