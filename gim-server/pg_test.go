package main

import (
	"context"
	"fmt"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
	"testing"
	"time"
)

type SerTest struct {
	ID        int64     `json:"id" gorm:"primaryKey;autoIncrement"`
	Name      string    `json:"name"`
	Age       int       `json:"age"`
	CreatedAt time.Time `json:"created_at"`
}

func (t *SerTest) TableName() string {
	return "ser_test"
}

func TestAesEn(t *testing.T) {
	url := "host=172.16.3.43 port=5432 user=authplatform_owner dbname=datacenter password=Fp24W8eSB7dDVGNDTVmYDmBj"
	db, err := gorm.Open(postgres.Open(url), &gorm.Config{
		Logger: logger.Default,
	})
	if err != nil {
		panic(err)
	}
	myDB := db.WithContext(context.Background())
	lst := make([]*SerTest, 0)
	//for i := 0;i < 10;i++ {
	//	lst = append(lst, &SerTest{
	//		ID:        0,
	//		Name:      fmt.Sprintf("龙龙%v", i),
	//		Age:       13+i,
	//	})
	//}
	//myDB.CreateInBatches(lst, 5)
	//for _, v := range lst {
	//	fmt.Printf("%+v\n", v)
	//	fmt.Println("========================")
	//}

	//myDB.Create(obj)
	//fmt.Printf("%+v", obj)
	//obj := new(SerTest)
	//myDB.First(obj, "1=1")
	//fmt.Printf("%+v", obj)

	myDB.Where("id > ?", 2).Offset(3).Limit(5).Find(&lst)
	for _, v := range lst {
		fmt.Printf("%+v\n", v)
		fmt.Println("========================")
	}

}

func TestTimeFmt(t *testing.T) {
	now := time.Now().Format("20060102150405000")
	fmt.Println(now)
}
