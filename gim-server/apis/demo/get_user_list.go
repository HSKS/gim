package demo

import (
	"gim-server/consts"
	"gim-server/errs"
	"gim-server/model"
	"gim-server/sdk"
	"gim-server/utils"
	"github.com/gin-gonic/gin"
)

type GetUserListArgs struct {
	PageNo   int `json:"page_no"`
	PageSize int `json:"page_size"`
}

func getUserList(ctx *gin.Context) {
	var (
		err  error
		args = new(GetUserListArgs)
	)
	if err = ctx.ShouldBindJSON(args); err != nil {
		utils.ERR(ctx, &utils.ErrArgs{
			Code: errs.ApiParamsErr,
			Err:  err,
		})
		return
	}
	if args.PageNo <= 0 {
		args.PageNo = 1
	}
	if args.PageSize <= 0 {
		args.PageSize = consts.PageSize
	}
	db := sdk.DB(ctx)
	list := make([]*model.TbUserDemo, 0)
	db.Where("reg_step = ? and uid != ?", 100, ctx.GetString(consts.UID)).
		Order("last_active desc").
		Offset((args.PageNo - 1) * args.PageSize).Limit(args.PageSize).Find(&list)
	utils.SUC(ctx, utils.PageListDto{
		Arr:      list,
		PageNo:   args.PageNo,
		PageSize: args.PageSize,
	})
}
