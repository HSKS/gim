package demo

import (
	"gim-server/errs"
	"gim-server/model"
	"gim-server/sdk"
	"gim-server/utils"
	"github.com/gin-gonic/gin"
)

type GetConversationInfosArgs struct {
	UIDs []string `json:"uids"`
}

type GetConversationInfosDto struct {
	UserInfos map[string]*model.TbUserDemo `json:"user_infos"`
}

func getConversationInfos(ctx *gin.Context) {
	var (
		err  error
		args = new(GetConversationInfosArgs)
	)
	if err = ctx.ShouldBindJSON(args); err != nil {
		utils.ERR(ctx, &utils.ErrArgs{
			Code: errs.ApiParamsErr,
			Err:  err,
		})
		return
	}
	db := sdk.DB(ctx)
	users := make([]*model.TbUserDemo, 0)
	db.Select("id,uid,nickname,avatar,gender").Where("uid in ?", args.UIDs).Find(&users)
	infos := make(map[string]*model.TbUserDemo)
	for _, user := range users {
		infos[user.UID] = user
	}
	utils.SUC(ctx, &GetConversationInfosDto{UserInfos: infos})
}
