package demo

import (
	"gim-server/middle"
	"github.com/gin-gonic/gin"
)

func ApiReg(engine *gin.Engine) {
	api := engine.Group("/gim/demo")
	{
		api.POST("/editProfile", middle.AppAuth(), editProfile)
		api.POST("/getConversationInfos", middle.AppAuth(), getConversationInfos)
		api.POST("/getUserList", middle.AppAuth(), getUserList)
		api.POST("/userLogin", userLogin)
	}
}
