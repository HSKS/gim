package c2c

import (
	"errors"
	"gim-server/apis/ws"
	"gim-server/consts"
	"gim-server/errs"
	"gim-server/model"
	"gim-server/sdk"
	"gim-server/utils"
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type EditC2cMsg struct {
	MsgID   int64  `json:"msg_id"`
	MsgData string `json:"msg_data"`
}

func editC2cMsg(ctx *gin.Context) {
	var (
		err  error
		args = new(EditC2cMsg)
	)
	if err = ctx.ShouldBindJSON(args); err != nil {
		utils.ERR(ctx, &utils.ErrArgs{
			Code: errs.ApiParamsErr,
			Err:  err,
		})
		return
	}
	db := sdk.DB(ctx)
	msg := new(model.TbC2cMsgRecord)
	if err := db.First(msg, "id = ?", args.MsgID).Error; err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			utils.ERR(ctx, &utils.ErrArgs{
				Code: errs.ApiRecordNotFound,
				Err:  err,
			})
		} else {
			utils.ERR(ctx, &utils.ErrArgs{
				Code: errs.ApiDbErr,
				Err:  err,
			})
		}
		return
	}
	msg.MsgData = args.MsgData
	if err := db.Save(msg).Error; err != nil {
		utils.ERR(ctx, &utils.ErrArgs{
			Code: errs.ApiDbErr,
			Err:  err,
		})
		return
	}
	_ = sdk.AsyncPool().Submit(func() {
		if ctx.GetString(consts.UID) == msg.FromUID {
			ws.EditC2cMsg(msg.ToUID, msg)
		} else {
			ws.EditC2cMsg(msg.FromUID, msg)
		}
	})
	utils.SUC(ctx, msg)
}
