package c2c

import (
	"gim-server/consts"
	"gim-server/errs"
	"gim-server/model"
	"gim-server/sdk"
	"gim-server/utils"
	"github.com/gin-gonic/gin"
)

type GetC2cMsgListArgs struct {
	ToUID    string `json:"to_uid"`
	LastID   int64  `json:"last_id"`
	PageSize int    `json:"page_size"`
}

func getC2cMsgList(ctx *gin.Context) {
	var (
		err  error
		args = new(GetC2cMsgListArgs)
	)
	if err = ctx.ShouldBindJSON(args); err != nil {
		utils.ERR(ctx, &utils.ErrArgs{
			Code: errs.ApiParamsErr,
			Err:  err,
		})
		return
	}
	if args.LastID <= 0 {
		args.LastID = consts.LastID
	}
	if args.PageSize <= 0 {
		args.PageSize = consts.PageSize
	}
	db := sdk.DB(ctx)
	list := make([]*model.TbC2cMsgRecord, 0)
	db.Where("id < ? and ((from_uid = ? and to_uid = ?) or (from_uid = ? and to_uid = ?)) and del_uid in ?",
		args.LastID, ctx.GetString(consts.UID), args.ToUID, args.ToUID, ctx.GetString(consts.UID), []string{"0", args.ToUID}).
		Order("id desc").Limit(args.PageSize).Find(&list)
	var lastId int64 = -1
	if len(list) > 0 {
		lastId = list[len(list)-1].ID
	}
	utils.SUC(ctx, &utils.PageListDto{
		Arr:      list,
		PageSize: args.PageSize,
		LastId:   lastId,
	})
}
