package c2c

import (
	"gim-server/middle"
	"github.com/gin-gonic/gin"
)

func ApiReg(engine *gin.Engine) {
	api := engine.Group("/gim/c2c", middle.AppAuth())
	{
		api.POST("/delC2cMsg", delC2cMsg)
		api.POST("/editC2cMsg", editC2cMsg)
		api.POST("/getC2cMsgList", getC2cMsgList)
		api.POST("/getUnreadCount", getUnreadCount)
		api.POST("/markC2cMsgRead", markC2cMsgRead)
		api.POST("/sendC2cMsg", sendC2cMsg)
		api.POST("/withdrawC2cMsg", withdrawC2cMsg)
	}
}
