package c2c

import (
	"gim-server/consts"
	"gim-server/errs"
	"gim-server/model"
	"gim-server/sdk"
	"gim-server/utils"
	"github.com/gin-gonic/gin"
)

type GetUnreadCount struct {
	FromUID string `json:"from_uid"`
}

func getUnreadCount(ctx *gin.Context) {
	var (
		err  error
		args = new(GetUnreadCount)
	)
	if err = ctx.ShouldBindJSON(args); err != nil {
		utils.ERR(ctx, &utils.ErrArgs{
			Code: errs.ApiParamsErr,
			Err:  err,
		})
		return
	}
	db := sdk.DB(ctx)
	var n int64
	if args.FromUID != "" {
		db.Model(new(model.TbC2cMsgRecord)).Where("from_uid = ? and to_uid = ? and readed = ? and del_uid not in ?", args.FromUID, ctx.GetString(consts.UID), false, []string{"-1", ctx.GetString(consts.UID)}).Count(&n)
	} else {
		db.Model(new(model.TbC2cMsgRecord)).Where("to_uid = ? and readed = ? and del_uid not in ?", ctx.GetString(consts.UID), false, []string{"-1", ctx.GetString(consts.UID)}).Count(&n)
	}
	utils.SUC(ctx, n)
}
