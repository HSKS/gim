package open

import (
	"gim-server/middle"
	"github.com/gin-gonic/gin"
)

func ApiReg(engine *gin.Engine) {
	api := engine.Group("/gim/open", middle.OpenAuth())
	{
		api.POST("/genToken", genToken)
		api.POST("/sendC2cMsg", sendC2cMsg)
	}
}
