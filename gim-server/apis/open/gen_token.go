package open

import (
	"gim-server/config"
	"gim-server/consts"
	"gim-server/errs"
	"gim-server/utils"
	"github.com/gin-gonic/gin"
)

type GenTokenArgs struct {
	UID string `json:"uid"`
}

func genToken(ctx *gin.Context) {
	var (
		err  error
		args = new(GenTokenArgs)
	)
	if err = ctx.ShouldBindJSON(args); err != nil {
		utils.ERR(ctx, &utils.ErrArgs{
			Code: errs.ApiParamsErr,
			Err:  err,
		})
		return
	}
	token, err := utils.CreateToken(config.GetCfg().SignKey, map[string]interface{}{
		consts.UID:   args.UID,
		consts.AppID: ctx.GetString(consts.AppID),
	})
	if err != nil {
		utils.ERR(ctx, &utils.ErrArgs{
			Code: errs.ApiSrvErr,
			Err:  err,
		})
		return
	}
	utils.SUC(ctx, token)
}
