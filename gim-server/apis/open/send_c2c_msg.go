package open

import (
	"errors"
	"gim-server/apis/ws"
	"gim-server/consts"
	"gim-server/errs"
	"gim-server/model"
	"gim-server/sdk"
	"gim-server/utils"
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
	"time"
)

type SendC2cMsgArgs struct {
	FromUID     string `json:"from_uid"`
	ToUID       string `json:"to_uid"`
	SyncFrom    bool   `json:"sync_from"`
	MsgType     string `json:"msg_type"`
	MsgData     string `json:"msg_data"`
	Ext         string `json:"ext"`
	NotPush     bool   `json:"not_push"`
	PushTitle   string `json:"push_title"`
	PushContent string `json:"push_content"`
}

func sendC2cMsg(ctx *gin.Context) {
	var (
		err  error
		args = new(SendC2cMsgArgs)
	)
	if err = ctx.ShouldBindJSON(args); err != nil {
		utils.ERR(ctx, &utils.ErrArgs{
			Code: errs.ApiParamsErr,
			Err:  err,
		})
		return
	}
	msg := new(model.TbC2cMsgRecord)
	msg.FromUID = ctx.GetString(consts.UID)
	msg.ToUID = args.ToUID
	msg.MsgType = args.MsgType
	msg.MsgData = args.MsgData
	msg.Code = model.SucCode
	msg.Ext = args.Ext
	msg.Readed = false
	msg.ErrTip = ""
	if !args.SyncFrom {
		msg.DelUID = args.FromUID
	} else {
		msg.DelUID = "0"
	}
	msg.CreatedAt = time.Now().Unix()
	db := sdk.DB(ctx)
	err = db.Transaction(func(tx *gorm.DB) error {
		cvs := new(model.TbConversation)
		if args.SyncFrom {
			if err := db.First(cvs, "from_uid = ? and to_uid = ?", msg.FromUID, msg.ToUID).Error; err != nil {
				if errors.Is(err, gorm.ErrRecordNotFound) {
					//新建
					cvs.FromUID = msg.FromUID
					cvs.ToUID = msg.ToUID
					cvs.UnreadCount = 0
					cvs.MarkTop = false
					cvs.CreatedAt = time.Now().Unix()
					cvs.UpdatedAt = time.Now().Unix()
					if err := tx.Create(cvs).Error; err != nil {
						return err
					}
				} else {
					return err
				}
			} else {
				//更新
				cvs.UpdatedAt = time.Now().Unix()
				if err := tx.Save(cvs).Error; err != nil {
					return err
				}
			}
		}

		if err := tx.Create(msg).Error; err != nil {
			return err
		}
		cvs = new(model.TbConversation)
		if err := db.First(cvs, "from_uid = ? and to_uid = ?", msg.ToUID, msg.FromUID).Error; err != nil {
			if errors.Is(err, gorm.ErrRecordNotFound) {
				//新建
				cvs.FromUID = msg.ToUID
				cvs.ToUID = msg.FromUID
				cvs.UnreadCount = 1
				cvs.MarkTop = false
				cvs.CreatedAt = time.Now().Unix()
				cvs.UpdatedAt = time.Now().Unix()
				if err := tx.Create(cvs).Error; err != nil {
					return err
				}
			} else {
				return err
			}
		} else {
			//更新
			cvs.UnreadCount = cvs.UnreadCount + 1
			cvs.UpdatedAt = time.Now().Unix()
			if err := tx.Save(cvs).Error; err != nil {
				return err
			}
		}
		return nil
	})
	if err != nil {
		utils.ERR(ctx, &utils.ErrArgs{
			Code: errs.ApiSrvErr,
			Err:  err,
		})
		return
	}
	_ = sdk.AsyncPool().Submit(func() {
		ws.SendC2cMsg(msg, args.NotPush, args.PushTitle, args.PushContent)
	})
	utils.SUC(ctx, msg)
}
