package rtc

import (
	"fmt"
	"gim-server/config"
	"gim-server/errs"
	"gim-server/sdk/artc"
	"gim-server/utils"
	rtctokenbuilder "github.com/AgoraIO/Tools/DynamicKey/AgoraDynamicKey/go/src/RtcTokenBuilder"
	"github.com/gin-gonic/gin"
)

type GetArtcTokenDto struct {
	AppID string `json:"app_id"`
	Token string `json:"token"`
	UID   uint32 `json:"uid"`
}

type GetArtcTokenArgs struct {
	UID         uint32 `json:"uid"`
	ChannelName string `json:"channel_name"`
	Role        uint16 `json:"role"`
}

func getArtcToken(ctx *gin.Context) {
	var (
		err  error
		args = new(GetArtcTokenArgs)
	)
	if err = ctx.ShouldBindJSON(args); err != nil {
		utils.ERR(ctx, &utils.ErrArgs{
			Code: errs.ApiParamsErr,
			Err:  err,
		})
		return
	}
	fmt.Printf("参数 ===>>> %+v\n", args)
	token, err := artc.GenerateIntUidRtcToken(args.UID, args.ChannelName, rtctokenbuilder.Role(args.Role))
	if err != nil {
		utils.ERR(ctx, &utils.ErrArgs{
			Code: errs.ApiSrvErr,
			Err:  err,
		})
		return
	}
	utils.SUC(ctx, &GetArtcTokenDto{
		AppID: config.GetCfg().ArtcAppID,
		Token: token,
		UID:   args.UID,
	})
}
