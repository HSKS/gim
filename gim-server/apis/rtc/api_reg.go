package rtc

import (
	"gim-server/middle"
	"github.com/gin-gonic/gin"
)

func ApiReg(engine *gin.Engine) {
	api := engine.Group("/gim/rtc")
	{
		api.POST("/getTrtcToken", middle.AppAuth(), getTrtcToken)
		api.POST("/getArtcToken", middle.AppAuth(), getArtcToken)
		api.POST("/callback", trtcCallback)
	}
}
