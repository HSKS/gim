package rtc

import (
	"fmt"
	"gim-server/utils"
	"github.com/gin-gonic/gin"
	"io/ioutil"
)

func trtcCallback(ctx *gin.Context) {
	body, err := ioutil.ReadAll(ctx.Request.Body)
	if err != nil {
		fmt.Printf("trtcCallbadk ERR => %+v\n", err)
	}
	fmt.Printf("trtcCallbadk Data => %+v\n", string(body))
	utils.SUC(ctx, nil)
}
