package rtc

import (
	"gim-server/config"
	"gim-server/consts"
	"gim-server/errs"
	"gim-server/sdk/trtc"
	"gim-server/utils"
	"github.com/gin-gonic/gin"
)

type GetTrtcTokenDto struct {
	AppID int    `json:"app_id"`
	Token string `json:"token"`
	UID   string `json:"uid"`
}

func getTrtcToken(ctx *gin.Context) {
	cfg := config.GetCfg()
	sig, err := trtc.GenUserSig(cfg.TrtcAppID, cfg.TrtcSignKey, ctx.GetString(consts.UID), 600)
	if err != nil {
		utils.ERR(ctx, &utils.ErrArgs{
			Code: errs.ApiSrvErr,
			Err:  err,
		})
		return
	}
	utils.SUC(ctx, &GetTrtcTokenDto{
		AppID: cfg.TrtcAppID,
		Token: sig,
		UID:   ctx.GetString(consts.UID),
	})
}
