package signaling

import (
	"gim-server/apis/ws"
	"gim-server/errs"
	"gim-server/utils"
	"github.com/gin-gonic/gin"
)

type CancelArgs struct {
	SignalingID int64       `json:"signaling_id"`
	ExtData     interface{} `json:"ext_data"`
	PushTitle   string      `json:"push_title"`
	PushAccount string      `json:"push_account"`
}

func cancel(ctx *gin.Context) {
	var (
		err  error
		args = new(CancelArgs)
	)
	if err = ctx.ShouldBindJSON(args); err != nil {
		utils.ERR(ctx, &utils.ErrArgs{
			Code: errs.ApiParamsErr,
			Err:  err,
		})
		return
	}
	if err = ws.SignalingCancelFn(args.SignalingID, args.PushTitle, args.PushAccount, args.ExtData); err != nil {
		utils.ERR(ctx, &utils.ErrArgs{
			Code: errs.ApiSrvErr,
			Err:  err,
		})
		return
	}
	utils.SUC(ctx, true)
}
