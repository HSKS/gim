package signaling

import (
	"gim-server/apis/ws"
	"gim-server/errs"
	"gim-server/utils"
	"github.com/gin-gonic/gin"
)

type RejectArgs struct {
	SignalingID int64       `json:"signaling_id"`
	ExtData     interface{} `json:"ext_data"`
}

func reject(ctx *gin.Context) {
	var (
		err  error
		args = new(RejectArgs)
	)
	if err = ctx.ShouldBindJSON(args); err != nil {
		utils.ERR(ctx, &utils.ErrArgs{
			Code: errs.ApiParamsErr,
			Err:  err,
		})
		return
	}
	if err = ws.SignalingRejectFn(args.SignalingID, args.ExtData); err != nil {
		utils.ERR(ctx, &utils.ErrArgs{
			Code: errs.ApiSrvErr,
			Err:  err,
		})
		return
	}
	utils.SUC(ctx, true)
}
