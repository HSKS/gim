package signaling

import (
	"gim-server/middle"
	"github.com/gin-gonic/gin"
)

func ApiReg(engine *gin.Engine) {
	api := engine.Group("/gim/signaling", middle.AppAuth())
	{
		api.POST("/accept", accept)
		api.POST("/cancel", cancel)
		api.POST("/invite", invite)
		api.POST("/reject", reject)
		api.POST("/checkLastSignaling", checkLastSignaling)
	}
}
