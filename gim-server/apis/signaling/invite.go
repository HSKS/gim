package signaling

import (
	"gim-server/apis/ws"
	"gim-server/consts"
	"gim-server/errs"
	"gim-server/utils"
	"github.com/gin-gonic/gin"
)

type InviteArgs struct {
	UID         string      `json:"uid"`
	ExtData     interface{} `json:"ext_data"`
	PushTitle   string      `json:"push_title"`
	PushAccount string      `json:"push_account"`
}

func invite(ctx *gin.Context) {
	var (
		err  error
		args = new(InviteArgs)
	)
	if err = ctx.ShouldBindJSON(args); err != nil {
		utils.ERR(ctx, &utils.ErrArgs{
			Code: errs.ApiParamsErr,
			Err:  err,
		})
		return
	}
	sid, err := ws.SignalingInviteFn(ctx.GetString(consts.UID), args.UID, args.ExtData, args.PushTitle, args.PushAccount)
	if err != nil {
		utils.ERR(ctx, &utils.ErrArgs{
			Code: errs.ApiSrvErr,
			Err:  err,
		})
		return
	}
	utils.SUC(ctx, sid)
}
