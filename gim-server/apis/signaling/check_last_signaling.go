package signaling

import (
	"gim-server/apis/ws"
	"gim-server/consts"
	"gim-server/utils"
	"github.com/gin-gonic/gin"
)

func checkLastSignaling(ctx *gin.Context) {
	ws.CheckLastSignaling(ctx.GetString(consts.UID))
	utils.SUC(ctx, true)
}
