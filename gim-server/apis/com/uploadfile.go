package com

import (
	"fmt"
	"gim-server/config"
	"gim-server/consts"
	"gim-server/errs"
	"gim-server/utils"
	"github.com/gin-gonic/gin"
	"strings"
	"time"
)

func uploadfile(ctx *gin.Context) {
	f, _ := ctx.FormFile("file")
	cfg := config.GetCfg()
	arr := strings.Split(f.Filename, ".")
	fileName := fmt.Sprintf("%v-%v-%v.%v", ctx.GetString(consts.UID), time.Now().Format("20060102150405"), utils.GetRandNumStr(6), arr[len(arr)-1])
	err := ctx.SaveUploadedFile(f, fmt.Sprintf("%v/%v", cfg.UploadFilePath, fileName))
	if err != nil {
		utils.ERR(ctx, &utils.ErrArgs{
			Code: errs.ApiSrvErr,
			Err:  err,
			Msg:  "upload failure",
		})
		return
	}
	utils.SUC(ctx, fmt.Sprintf("/gim/files/%v", fileName))
}
