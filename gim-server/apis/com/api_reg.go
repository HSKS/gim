package com

import (
	"gim-server/middle"
	"github.com/gin-gonic/gin"
)

func ApiReg(engine *gin.Engine) {
	api := engine.Group("/gim/com")
	{
		api.POST("/resetOnFrontDesk", middle.AppAuth(), resetOnFrontDesk)
		api.POST("/genCosUploadUrl", middle.AppAuth(), genCosUploadUrl)
		api.POST("/ipgeo", ipgeo)
		api.POST("/uploadfile", middle.AppAuth(), uploadfile)
	}
}
