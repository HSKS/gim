package com

import (
	"fmt"
	"gim-server/apis/ws"
	"gim-server/consts"
	"gim-server/errs"
	"gim-server/utils"
	"github.com/gin-gonic/gin"
)

type ResetOnFrontDeskArgs struct {
	Flag bool `json:"flag"`
}

func resetOnFrontDesk(ctx *gin.Context) {
	var (
		err  error
		args = new(ResetOnFrontDeskArgs)
	)
	if err = ctx.ShouldBindJSON(args); err != nil {
		utils.ERR(ctx, &utils.ErrArgs{
			Code: errs.ApiParamsErr,
			Err:  err,
		})
		return
	}
	fmt.Printf("%s OnFrontDesk: %v", ctx.GetString(consts.UID), args.Flag)
	if err = ws.ResetOnFrontDesk(ctx.GetString(consts.UID), args.Flag); err != nil {
		utils.ERR(ctx, &utils.ErrArgs{
			Code: errs.ApiSrvErr,
			Err:  err,
		})
		return
	}
	utils.SUC(ctx, true)
}
