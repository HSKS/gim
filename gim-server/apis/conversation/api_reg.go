package conversation

import (
	"gim-server/middle"
	"github.com/gin-gonic/gin"
)

func ApiReg(engine *gin.Engine) {
	api := engine.Group("/gim/conversation", middle.AppAuth())
	{
		api.POST("/delConversation", delConversation)
		api.POST("/getConversationList", getConversationList)
		api.POST("/markConversationTop", markConversationTop)
		api.POST("/checkMarkTop", checkMarkTop)
	}
}
