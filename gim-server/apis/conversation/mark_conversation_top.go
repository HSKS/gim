package conversation

import (
	"gim-server/consts"
	"gim-server/errs"
	"gim-server/model"
	"gim-server/sdk"
	"gim-server/utils"
	"github.com/gin-gonic/gin"
)

type MarkConversationTopArgs struct {
	MarkTop        bool   `json:"mark_top"`
	ToUID          string `json:"to_uid"`
	ConversationID int64  `json:"conversation_id"`
}

func markConversationTop(ctx *gin.Context) {
	var (
		err  error
		args = new(MarkConversationTopArgs)
	)
	if err = ctx.ShouldBindJSON(args); err != nil {
		utils.ERR(ctx, &utils.ErrArgs{
			Code: errs.ApiParamsErr,
			Err:  err,
		})
		return
	}
	db := sdk.DB(ctx)
	if args.ConversationID > 0 {
		if err = db.Model(new(model.TbConversation)).Where("id = ?", args.ConversationID).UpdateColumn("mark_top", args.MarkTop).Error; err != nil {
			utils.ERR(ctx, &utils.ErrArgs{
				Code: errs.ApiDbErr,
				Err:  err,
			})
			return
		}
	} else if args.ToUID != "" {
		if err := db.Model(new(model.TbConversation)).Where("from_uid = ? and to_uid = ?", ctx.GetString(consts.UID), args.ToUID).UpdateColumn("mark_top", args.MarkTop).Error; err != nil {
			utils.ERR(ctx, &utils.ErrArgs{
				Code: errs.ApiDbErr,
				Err:  err,
			})
			return
		}
	}

	utils.SUC(ctx, true)
}
