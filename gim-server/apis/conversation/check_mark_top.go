package conversation

import (
	"gim-server/consts"
	"gim-server/errs"
	"gim-server/model"
	"gim-server/sdk"
	"gim-server/utils"
	"github.com/gin-gonic/gin"
)

type CheckMarkTopArgs struct {
	ToUID          string `json:"to_uid"`
	ConversationID int64  `json:"conversation_id"`
}

func checkMarkTop(ctx *gin.Context) {
	var (
		err  error
		args = new(CheckMarkTopArgs)
	)
	if err = ctx.ShouldBindJSON(args); err != nil {
		utils.ERR(ctx, &utils.ErrArgs{
			Code: errs.ApiParamsErr,
			Err:  err,
		})
		return
	}
	db := sdk.DB(ctx)
	var n int64
	if args.ConversationID > 0 {
		db.Model(new(model.TbConversation)).Where("id = ? and mark_top = ?", args.ConversationID, true).Count(&n)
	} else if args.ToUID != "" {
		db.Model(new(model.TbConversation)).Where("from_uid = ? and to_uid = ? and mark_top = ?", ctx.GetString(consts.UID), args.ToUID, true).Count(&n)
	}
	utils.SUC(ctx, n > 0)
}
