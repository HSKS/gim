package ws

import (
	"context"
	"gim-server/model"
	"gim-server/sdk"
)

func WithdrawC2cMsg(uid string, msgID int64, changeBadge bool) {
	sendMsg(uid, &MsgDto{
		Event: SybWithdrawC2cMsg,
		Data:  msgID,
	})
	if changeBadge {
		//静默修改推送数量
		db := sdk.DB(context.Background())
		var n int64
		db.Model(new(model.TbC2cMsgRecord)).Where("to_uid = ? and readed = ? and del_uid not in ?", uid, false, []string{"-1", uid}).Count(&n)
		TpnsPush(uid, "", "", int(n), 2)
	}
}
