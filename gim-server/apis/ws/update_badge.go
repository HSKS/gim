package ws

import (
	"context"
	"gim-server/model"
	"gim-server/sdk"
)

func UpdateBadge(uid string) {
	db := sdk.DB(context.Background())
	var n int64
	db.Model(new(model.TbC2cMsgRecord)).Where("to_uid = ? and readed = ? and del_uid not in ?", uid, false, []string{"-1", uid}).Count(&n)
	TpnsPush(uid, "", "", int(n), 2)
}
