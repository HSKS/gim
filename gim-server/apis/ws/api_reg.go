package ws

import (
	"github.com/gin-gonic/gin"
)

func ApiReg(engine *gin.Engine) {
	engine.GET("/gim/ws/socket", ws)
}
