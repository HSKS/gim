package ws

import (
	"context"
	"fmt"
	"gim-server/sdk"
	"gim-server/utils"
	"time"
)

func ResetOnFrontDesk(uid string, flag bool) error {
	rds := sdk.Redis(context.Background())
	userStr := rds.Get(fmt.Sprintf(imKeyFmt, uid)).Val()
	if userStr != "" {
		user := new(connInfo)
		err := utils.JsonUnmarshal([]byte(userStr), user)
		if err != nil {
			return err
		}
		user.OnFrontDesk = flag
		bt, err := utils.JsonMarshal(user)
		if err != nil {
			return err
		}
		return rds.Set(fmt.Sprintf(imKeyFmt, uid), string(bt), time.Hour*24*365).Err()
	}
	return nil
}
