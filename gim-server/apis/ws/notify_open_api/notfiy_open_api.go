package notify_open_api

import (
	"gim-server/comsvc"
	"gim-server/utils"
)

type NotifyDto struct {
	Code    string      `json:"code"`
	Message interface{} `json:"message"`
	Data    interface{} `json:"data"`
}

type NotifyOnlineArgs struct {
	AppID  string `json:"app_id"`
	UID    string `json:"uid"`
	Online bool   `json:"online"`
}

type NotifyBeforeSendC2cMsgArgs struct {
	AppID   string `json:"app_id"`
	FromUID string `json:"from_uid"`
	ToUID   string `json:"to_uid"`
	MsgType string `json:"msg_type"`
	MsgData string `json:"msg_data"`
}

func NotifyOnline(args *NotifyOnlineArgs) {
	app, err := comsvc.GetAppInfo(args.AppID)
	if err != nil {
		return
	}
	if app.CbURL == "" {
		return
	}
	_, _ = utils.HttpPost(app.CbURL, args, map[string]string{
		"event": "NotifyOnline",
	})
}

func NotifyBeforeSendC2cMsg(args *NotifyBeforeSendC2cMsgArgs) (*NotifyDto, error) {
	app, err := comsvc.GetAppInfo(args.AppID)
	if err != nil {
		return nil, nil
	}
	if app.CbURL == "" {
		return nil, nil
	}
	body, err := utils.HttpPost(app.CbURL, args, map[string]string{
		"event": "NotifyBeforeSendC2cMsg",
	})
	if err != nil {
		return nil, err
	}
	dto := new(NotifyDto)
	err = utils.JsonUnmarshal(body, dto)
	if err != nil {
		return nil, err
	}
	return dto, nil
}
