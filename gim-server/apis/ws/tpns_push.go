package ws

import (
	"context"
	"fmt"
	"gim-server/comsvc"
	"gim-server/sdk"
	"gim-server/utils"
)

/*
TPNS设置
badgeType
-2：自动增加1，支持华为设备
-1：不变，支持华为、vivo 设备
[0, 100)：直接设置，支持华为、vivo 设备
*/
func TpnsPush(uid string, pushTitle string, pushContent string, badgeType int, notifyType int) {
	if uid == "" {
		return
	}
	rds := sdk.Redis(context.Background())
	userStr := rds.Get(fmt.Sprintf(imKeyFmt, uid)).Val()
	if userStr != "" {
		user := new(connInfo)
		err := utils.JsonUnmarshal([]byte(userStr), user)
		if err == nil {
			app, _ := comsvc.GetAppInfo(user.AppID)
			if app != nil {
				var notify bool
				if notifyType == 0 {
					notify = !user.OnFrontDesk
				} else if notifyType == 1 {
					notify = true
				} else if notifyType == 2 {
					notify = false
				}
				if notify {
					if pushTitle == "" || pushContent == "" {
						notify = false
					}
				}
				accessId := app.TpnsAndroidAccessId
				secretKey := app.TpnsAndroidSecretKey
				if user.Platform == "ios" {
					accessId = app.TpnsIosAccessId
					secretKey = app.TpnsAndroidSecretKey
				}
				deviceToken := comsvc.GetFirebaseDeviceToken(app.AppID, uid)
				_ = sdk.PushOneAccount(&sdk.PushOneAccountArgs{
					PushType:        app.PushType,
					IsProd:          app.IsProd,
					AccessId:        accessId,
					SecretKey:       secretKey,
					FirebaseAuthKey: app.FirebaseAuthKey,
					Notify:          notify,
					Account:         uid,
					DeviceToken:     []string{deviceToken},
					Title:           pushTitle,
					Content:         pushContent,
					BadgeType:       badgeType,
				})
			}
		}
	}
}
