package ws

import (
	"context"
	"gim-server/model"
	"gim-server/sdk"
)

func MarkC2cMsgRead(myUid, remoteUid string) {
	//IM通知对方我已读了
	_ = sendMsg(remoteUid, &MsgDto{
		Event: SybMarkMsgAsRead,
		Data:  myUid,
	})
	conv := new(model.TbConversation)
	db := sdk.DB(context.Background())
	if err := db.First(conv, "from_uid = ? and to_uid = ?", myUid, remoteUid).Error; err == nil {
		msg := new(model.TbC2cMsgRecord)
		if err := db.Where("((from_uid = ? and to_uid = ?) or (from_uid = ? and to_uid = ?)) and del_uid not in ?",
			myUid, remoteUid, remoteUid, myUid, []string{"-1", myUid}).
			Order("id desc").First(msg).Error; err == nil {
			conv.LastMsg = msg
		}
		_ = sendMsg(myUid, &MsgDto{
			Event: SybConversationUpdate,
			Data:  conv,
		})
	}
	//静默修改推送数量
	UpdateBadge(myUid)
}
