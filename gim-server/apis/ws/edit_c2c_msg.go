package ws

import (
	"gim-server/model"
)

func EditC2cMsg(remoteUid string, msg *model.TbC2cMsgRecord) {
	_ = sendMsg(remoteUid, &MsgDto{
		Event: SybEditC2cMsg,
		Data:  msg,
	})
}
