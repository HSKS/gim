package ws

const (
	SybConnected          = "SybConnected"
	SybConversationUpdate = "SybConversationUpdate"
	SybNewC2cMsg          = "SybNewC2cMsg"
	SybEditC2cMsg         = "SybEditC2cMsg"
	SybUnreadCount        = "SybUnreadCount"
	SybWithdrawC2cMsg     = "SybWithdrawC2cMsg"
	SybMarkMsgAsRead      = "SybMarkMsgAsRead"
	SybLogout             = "SybLogout"

	SignalingInvite  = "SignalingInvite"
	SignalingCancel  = "SignalingCancel"
	SignalingAccept  = "SignalingAccept"
	SignalingReject  = "SignalingReject"
	SignalingTimeout = "SignalingTimeout"
)
