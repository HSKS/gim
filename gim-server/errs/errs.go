package errs

const (
	ApiSrvErr          = "ApiSrvErr"
	ApiWithDrawTimeout = "ApiWithDrawTimeout"
	ApiTokenErr        = "ApiTokenErr"
	ApiParamsErr       = "ApiParamsErr"
	ApiDbErr           = "ApiDbErr"
	ApiRecordNotFound  = "ApiRecordNotFound"
)
