package consts

const (
	Token    = "token"
	UID      = "uid"
	Gender   = "gender"
	AppID    = "appId"
	Platform = "platform"
	AppInfo  = "appInfo"
)

const (
	ShouldLogger = "shouldLogger"
	Code         = "code"
	Method       = "method"
	ClientIP     = "clientIP"
	HostIP       = "hostIP"
	UserAgent    = "userAgent"
	Duration     = "duration"
	ReqParams    = "reqParams"
	RespBody     = "respBody"
	Error        = "error"
	Stack        = "stack"
	Message      = "message"
)

const (
	PageSize   = 20
	PageNumber = 1
	LastID     = 99999999999
)
