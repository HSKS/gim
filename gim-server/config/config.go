package config

import (
	"flag"
	"fmt"
	"github.com/jinzhu/configor"
	"sync"
	"time"
)

type Config struct {
	//服务基本配置
	ServerPort int    `json:"server_port" yaml:"ServerPort"`
	SignKey    string `json:"sign_key" yaml:"SignKey"`
	//数据库类型mysql/pg
	DBType string `json:"db_type" yaml:"DBType"`
	//mysql相关配置
	MysqlUrl  string `json:"mysql_url" yaml:"MysqlUrl"`
	MysqlUser string `json:"mysql_user" yaml:"MysqlUser"`
	MysqlPass string `json:"mysql_pass" yaml:"MysqlPass"`
	MysqlDB   string `json:"mysql_db" yaml:"MysqlDB"`
	//pg配置
	PgUrl  string `json:"pg_url" yaml:"PgUrl"`
	PgUser string `json:"pg_user" yaml:"PgUser"`
	PgPass string `json:"pg_pass" yaml:"PgPass"`
	PgDB   string `json:"pg_db" yaml:"PgDB"`
	//redis相关配置
	RedisUrl  string `json:"redis_url" yaml:"RedisUrl"`
	RedisPass string `json:"redis_pass" yaml:"RedisPass"`
	RedisDB   int    `json:"redis_db" yaml:"RedisDB"`
	//cos相关配置
	CosSecretId          string `json:"cos_secret_id" yaml:"CosSecretId"`
	CosSecretKey         string `json:"cos_secret_key" yaml:"CosSecretKey"`
	CosBucket            string `json:"cos_bucket" yaml:"CosBucket"`
	CosRegion            string `json:"cos_region" yaml:"CosRegion"`
	CosImageAuditBizType string `json:"cos_image_audit_biz_type" yaml:"CosImageAuditBizType"`
	CosTextAuditBizType  string `json:"cos_text_audit_biz_type" yaml:"CosTextAuditBizType"`
	CosImageFolder       string `json:"cos_image_folder" yaml:"CosImageFolder"`
	CosVideoFolder       string `json:"cos_video_folder" yaml:"CosVideoFolder"`
	CosAudioFolder       string `json:"cos_audio_folder" yaml:"CosAudioFolder"`
	//TRTC
	TrtcAppID   int    `json:"trtc_app_id" yaml:"TrtcAppID"`
	TrtcSignKey string `json:"trtc_sign_key" yaml:"TrtcSignKey"`
	//ARTC
	ArtcAppID       string `json:"artc_app_id" yaml:"ArtcAppID"`
	ArtcCertificate string `json:"artc_certificate" yaml:"ArtcCertificate"`
	//上传文件保存路径
	UploadFilePath string `json:"upload_file_path" yaml:"UploadFilePath"`
}

var (
	cfg  *Config
	once sync.Once
)

func GetCfg() *Config {
	once.Do(func() {
		cfgPath := flag.String("cfg", "./config.yaml", "配置文件")
		flag.Parse()
		cfg = new(Config)
		if err := configor.New(&configor.Config{AutoReload: true, AutoReloadInterval: time.Second * 5}).Load(cfg, *cfgPath); err != nil {
			fmt.Printf("读取配置文件出错了：%v\n", err.Error())
			panic(fmt.Sprintf("无效配置文件路径: %v\n", *cfgPath))
		}
		fmt.Printf(`
-----------------------config---------------------------
%+v
------------------------------------------------------
`, cfg)
	})
	return cfg
}
