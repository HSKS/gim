package utils

import (
	"bytes"
	"crypto/aes"
	"crypto/cipher"
	"crypto/md5"
	"encoding/hex"
	"errors"
)

//PKCS7 填充模式
func pkcS7Padding(ciphertext []byte, blockSize int) []byte {
	padding := blockSize - len(ciphertext)%blockSize
	//Repeat()函数的功能是把切片[]byte{byte(padding)}复制padding个，然后合并成新的字节切片返回
	padtext := bytes.Repeat([]byte{byte(padding)}, padding)
	return append(ciphertext, padtext...)
}

//填充的反向操作，删除填充字符串
func pkcS7UnPadding(origData []byte) ([]byte, error) {
	//获取数据长度
	length := len(origData)
	if length == 0 {
		return nil, errors.New("加密字符串错误！")
	} else {
		//获取填充字符串长度
		unpadding := int(origData[length-1])
		//截取切片，删除填充字节，并且返回明文
		return origData[:(length - unpadding)], nil
	}
}

//实现加密
func AesEn(origData string, key string) (string, error) {
	//创建加密算法实例
	block, err := aes.NewCipher([]byte(key))
	if err != nil {
		return "", err
	}
	//获取块的大小
	blockSize := block.BlockSize()
	//对数据进行填充，让数据长度满足需求
	origData = string(pkcS7Padding([]byte(origData), blockSize))
	//采用AES加密方法中CBC加密模式
	blocMode := cipher.NewCBCEncrypter(block, []byte(key)[:blockSize])
	crypted := make([]byte, len(origData))
	//执行加密
	blocMode.CryptBlocks(crypted, []byte(origData))
	return hex.EncodeToString(crypted), nil
}

//实现解密
func AesDe(cypted string, key string) (string, error) {
	cyptedBt, err := hex.DecodeString(cypted)
	if err != nil {
		return "", err
	}
	//创建加密算法实例
	block, err := aes.NewCipher([]byte(key))
	if err != nil {
		return "", err
	}
	//获取块大小
	blockSize := block.BlockSize()
	//创建加密客户端实例
	blockMode := cipher.NewCBCDecrypter(block, []byte(key)[:blockSize])
	origData := make([]byte, len(cyptedBt))
	//这个函数也可以用来解密
	blockMode.CryptBlocks(origData, cyptedBt)
	//去除填充字符串
	origData, err = pkcS7UnPadding(origData)
	if err != nil {
		return "", err
	}
	return string(origData), err
}

func MD5WithoutKey(str string) string {
	h := md5.New()
	h.Write([]byte(str))
	return hex.EncodeToString(h.Sum(nil))
}
