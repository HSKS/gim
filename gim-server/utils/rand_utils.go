package utils

import (
	"fmt"
	"math/rand"
	"strings"
	"time"
)

const numLetterString = "0123456789"

const strLetterString = "0123456789qweertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM"

var rander = rand.New(rand.NewSource(time.Now().UnixNano()))

func GetRandNumStr(length int) string {
	str := []byte(numLetterString)
	res := fmt.Sprintf("%d", rander.Intn(9)+1)
	for i := 1; i < length; i++ {
		res += fmt.Sprintf("%c", str[rander.Intn(strings.Count(numLetterString, "")-1)])
	}
	return res
}

func GetRandLetter(length int) string {
	str := []byte(strLetterString)
	res := ""
	for i := 0; i < length; i++ {
		res += fmt.Sprintf("%c", str[rander.Intn(strings.Count(numLetterString, "")-1)])
	}
	return res
}

func GetRandInt32(max int) int {
	return rander.Intn(max)
}

func GetRandInt64(max int64) int64 {
	return rander.Int63n(max)
}
