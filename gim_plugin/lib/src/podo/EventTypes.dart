class EventTypes {
  static const SybConversationUpdate = "SybConversationUpdate";
  static const SybNewC2cMsg = "SybNewC2cMsg";
  static const SybEditC2cMsg = "SybEditC2cMsg";
  static const SybUnreadCount = "SybUnreadCount";
  static const SybWithdrawC2cMsg = "SybWithdrawC2cMsg";
  static const SybMarkMsgAsRead = "SybMarkMsgAsRead";

  static const SignalingInvite = "SignalingInvite";
  static const SignalingCancel = "SignalingCancel";
  static const SignalingAccept = "SignalingAccept";
  static const SignalingReject = "SignalingReject";
  static const SignalingTimeout = "SignalingTimeout";
}
