class MsgTypes {
  static const MsgTypeText = "text";
  static const MsgTypeImage = "image";
  static const MsgTypeVideo = "video";
  static const MsgTypeAudio = "audio";
  static const MsgTypeGeo = "geo";
  static const MsgTypeSignaling = "signaling";
  static const MsgTypeCustom = "custom";
}
