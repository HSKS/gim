class HttpResult {
  dynamic code;
  dynamic message;
  dynamic data;

  HttpResult({this.code, this.message, this.data});
}