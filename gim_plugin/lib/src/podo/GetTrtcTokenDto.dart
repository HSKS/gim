/// app_id : 0
/// token : ""
/// uid : ""

class GetTrtcTokenDto {
  GetTrtcTokenDto({
      this.appId, 
      this.token, 
      this.uid,});

  GetTrtcTokenDto.fromJson(dynamic json) {
    appId = json['app_id'];
    token = json['token'];
    uid = json['uid'];
  }
  int? appId;
  String? token;
  String? uid;
GetTrtcTokenDto copyWith({  int? appId,
  String? token,
  String? uid,
}) => GetTrtcTokenDto(  appId: appId ?? this.appId,
  token: token ?? this.token,
  uid: uid ?? this.uid,
);
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['app_id'] = appId;
    map['token'] = token;
    map['uid'] = uid;
    return map;
  }

}