/// event : ""
/// from_uid : ""
/// signaling_id : 0
/// expire : 0

class SignalingMsg {
  SignalingMsg({
    this.event,
    this.fromUid,
    this.extData,
    this.signalingId,
    this.expire,
  });

  SignalingMsg.fromJson(dynamic json) {
    event = json['event'];
    fromUid = json['from_uid'];
    extData = json['ext_data'];
    signalingId = json['signaling_id'];
    expire = json['expire'];
  }

  String? event;
  String? fromUid;
  dynamic extData;
  int? signalingId;
  int? expire;

  SignalingMsg copyWith({
    String? event,
    String? fromUid,
    dynamic extData,
    int? signalingId,
    int? expire,
  }) =>
      SignalingMsg(
        event: event ?? this.event,
        fromUid: fromUid ?? this.fromUid,
        extData: extData ?? this.extData,
        signalingId: signalingId ?? this.signalingId,
        expire: expire ?? this.expire,
      );

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['event'] = event;
    map['from_uid'] = fromUid;
    map['ext_data'] = extData;
    map['signaling_id'] = signalingId;
    map['expire'] = expire;
    return map;
  }
}
