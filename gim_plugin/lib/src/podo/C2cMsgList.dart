import 'package:gim_plugin/gim_plugin.dart';

/// arr : [{"name":""}]
/// page_size : 0
/// last_id : 0

class C2cMsgList {
  C2cMsgList({
    this.arr,
    this.pageSize,
    this.lastId,
  });

  C2cMsgList.fromJson(dynamic json) {
    if (json['arr'] != null) {
      arr = [];
      json['arr'].forEach((v) {
        arr?.add(C2cMsg.fromJson(v));
      });
    }
    pageSize = json['page_size'];
    lastId = json['last_id'];
  }

  List<C2cMsg>? arr;
  int? pageSize;
  int? lastId;

  C2cMsgList copyWith({
    List<C2cMsg>? arr,
    int? pageSize,
    int? lastId,
  }) =>
      C2cMsgList(
        arr: arr ?? this.arr,
        pageSize: pageSize ?? this.pageSize,
        lastId: lastId ?? this.lastId,
      );

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (arr != null) {
      map['arr'] = arr?.map((v) => v.toJson()).toList();
    }
    map['page_size'] = pageSize;
    map['last_id'] = lastId;
    return map;
  }
}
