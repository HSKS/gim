/// url : ""
/// width : 0
/// height : 0

class ImageMsg {
  ImageMsg({
    this.url,
    this.width,
    this.height,
  });

  ImageMsg.fromJson(dynamic json) {
    url = json['url'];
    width = json['width'];
    height = json['height'];
  }

  String? url;
  int? width;
  int? height;

  ImageMsg copyWith({
    String? url,
    int? width,
    int? height,
  }) =>
      ImageMsg(
        url: url ?? this.url,
        width: width ?? this.width,
        height: height ?? this.height,
      );

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['url'] = url;
    map['width'] = width;
    map['height'] = height;
    return map;
  }
}
