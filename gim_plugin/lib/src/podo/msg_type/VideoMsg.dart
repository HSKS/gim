/// url : ""
/// dur : 0
/// width : 0
/// height : 0

class VideoMsg {
  VideoMsg({
    this.url,
    this.cover,
    this.dur,
    this.width,
    this.height,
  });

  VideoMsg.fromJson(dynamic json) {
    url = json['url'];
    cover = json['cover'];
    dur = json['dur'];
    width = json['width'];
    height = json['height'];
  }

  String? url;
  String? cover;
  int? dur;
  int? width;
  int? height;

  VideoMsg copyWith({
    String? url,
    String? cover,
    int? dur,
    int? width,
    int? height,
  }) =>
      VideoMsg(
        url: url ?? this.url,
        cover: cover ?? this.cover,
        dur: dur ?? this.dur,
        width: width ?? this.width,
        height: height ?? this.height,
      );

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['url'] = url;
    map['cover'] = cover;
    map['dur'] = dur;
    map['width'] = width;
    map['height'] = height;
    return map;
  }
}
