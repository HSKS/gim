/// url : ""
/// dur : 0

class AudioMsg {
  AudioMsg({
      this.url, 
      this.dur,});

  AudioMsg.fromJson(dynamic json) {
    url = json['url'];
    dur = json['dur'];
  }
  String? url;
  int? dur;
AudioMsg copyWith({  String? url,
  int? dur,
}) => AudioMsg(  url: url ?? this.url,
  dur: dur ?? this.dur,
);
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['url'] = url;
    map['dur'] = dur;
    return map;
  }

}