/// lat : 0.0
/// lng : 0.0
/// country : ""
/// province : ""
/// city : ""
/// area : ""
/// addr : ""

class GeoMsg {
  GeoMsg({
    this.lat,
    this.lng,
    this.country,
    this.province,
    this.city,
    this.area,
    this.addr,
  });

  GeoMsg.fromJson(dynamic json) {
    lat = json['lat'];
    lng = json['lng'];
    country = json['country'];
    province = json['province'];
    city = json['city'];
    area = json['area'];
    addr = json['addr'];
  }

  double? lat;
  double? lng;
  String? country;
  String? province;
  String? city;
  String? area;
  String? addr;

  GeoMsg copyWith({
    double? lat,
    double? lng,
    String? country,
    String? province,
    String? city,
    String? area,
    String? addr,
  }) =>
      GeoMsg(
        lat: lat ?? this.lat,
        lng: lng ?? this.lng,
        country: country ?? this.country,
        province: province ?? this.province,
        city: city ?? this.city,
        area: area ?? this.area,
        addr: addr ?? this.addr,
      );

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['lat'] = lat;
    map['lng'] = lng;
    map['country'] = country;
    map['province'] = province;
    map['city'] = city;
    map['area'] = area;
    map['addr'] = addr;
    return map;
  }
}
