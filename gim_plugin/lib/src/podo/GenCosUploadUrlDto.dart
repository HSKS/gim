/// key : ""
/// url : ""

class GenCosUploadUrlDto {
  GenCosUploadUrlDto({
    this.key,
    this.url,
  });

  GenCosUploadUrlDto.fromJson(dynamic json) {
    key = json['key'];
    url = json['url'];
  }

  String? key;
  String? url;

  GenCosUploadUrlDto copyWith({
    String? key,
    String? url,
  }) =>
      GenCosUploadUrlDto(
        key: key ?? this.key,
        url: url ?? this.url,
      );

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['key'] = key;
    map['url'] = url;
    return map;
  }
}
