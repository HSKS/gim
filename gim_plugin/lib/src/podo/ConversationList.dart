import 'ConversationInfo.dart';

class ConversationList {
  ConversationList({
      this.arr, 
      this.pageNo, 
      this.pageSize,});

  ConversationList.fromJson(dynamic json) {
    if (json['arr'] != null) {
      arr = [];
      json['arr'].forEach((v) {
        arr?.add(ConversationInfo.fromJson(v));
      });
    }
    pageNo = json['page_no'];
    pageSize = json['page_size'];
  }
  List<ConversationInfo>? arr;
  int? pageNo;
  int? pageSize;
ConversationList copyWith({  List<ConversationInfo>? arr,
  int? pageNo,
  int? pageSize,
}) => ConversationList(  arr: arr ?? this.arr,
  pageNo: pageNo ?? this.pageNo,
  pageSize: pageSize ?? this.pageSize,
);
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (arr != null) {
      map['arr'] = arr?.map((v) => v.toJson()).toList();
    }
    map['page_no'] = pageNo;
    map['page_size'] = pageSize;
    return map;
  }

}
