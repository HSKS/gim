class MsgDto {
  MsgDto({
    this.event,
    this.data,
  });

  MsgDto.fromJson(dynamic json) {
    event = json['event'];
    data = json['data'];
  }

  String? event;
  dynamic data;

  MsgDto copyWith({
    String? event,
    dynamic data,
  }) =>
      MsgDto(
        event: event ?? this.event,
        data: data ?? this.data,
      );

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['event'] = event;
    map['data'] = data;
    return map;
  }
}
