/// app_id : ""
/// token : ""
/// uid : ""

class GetArtcTokenDto {
  GetArtcTokenDto({
      this.appId, 
      this.token, 
      this.uid,});

  GetArtcTokenDto.fromJson(dynamic json) {
    appId = json['app_id'];
    token = json['token'];
    uid = json['uid'];
  }
  String? appId;
  String? token;
  int? uid;
GetArtcTokenDto copyWith({  String? appId,
  String? token,
  int? uid,
}) => GetArtcTokenDto(  appId: appId ?? this.appId,
  token: token ?? this.token,
  uid: uid ?? this.uid,
);
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['app_id'] = appId;
    map['token'] = token;
    map['uid'] = uid;
    return map;
  }

}