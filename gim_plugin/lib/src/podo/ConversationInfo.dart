import 'package:gim_plugin/gim_plugin.dart';

class ConversationInfo {
  ConversationInfo({
    this.id,
    this.nickname,
    this.avatar,
    this.ext,
    this.fromUid,
    this.toUid,
    this.unreadCount,
    this.markTop,
    this.createdAt,
    this.updatedAt,
    this.lastMsg,
  });

  ConversationInfo.fromJson(dynamic json) {
    id = json['id'];
    nickname = json['nickname'];
    avatar = json['avatar'];
    ext = json['ext'];
    fromUid = json['from_uid'];
    toUid = json['to_uid'];
    unreadCount = json['unread_count'];
    markTop = json['mark_top'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    if (json['last_msg'] != null) {
      lastMsg = C2cMsg.fromJson(json['last_msg']);
    }
  }

  int? id;
  String? nickname;
  String? avatar;
  dynamic ext;
  String? fromUid;
  String? toUid;
  int? unreadCount;
  bool? markTop;
  int? createdAt;
  int? updatedAt;
  C2cMsg? lastMsg;

  ConversationInfo copyWith({
    int? id,
    String? nickname,
    String? avatar,
    dynamic ext,
    String? fromUid,
    String? toUid,
    int? unreadCount,
    bool? markTop,
    int? createdAt,
    int? updatedAt,
    C2cMsg,
    lastMsg,
  }) =>
      ConversationInfo(
        id: id ?? this.id,
        nickname: nickname ?? this.nickname,
        avatar: avatar ?? this.avatar,
        ext: ext ?? this.ext,
        fromUid: fromUid ?? this.fromUid,
        toUid: toUid ?? this.toUid,
        unreadCount: unreadCount ?? this.unreadCount,
        markTop: markTop ?? this.markTop,
        createdAt: createdAt ?? this.createdAt,
        updatedAt: updatedAt ?? this.updatedAt,
        lastMsg: lastMsg ?? this.lastMsg,
      );

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = id;
    map['nickname'] = nickname;
    map['avatar'] = avatar;
    map['ext'] = ext;
    map['from_uid'] = fromUid;
    map['to_uid'] = toUid;
    map['unread_count'] = unreadCount;
    map['mark_top'] = markTop;
    map['created_at'] = createdAt;
    map['updated_at'] = updatedAt;
    if (lastMsg != null) {
      map['last_msg'] = lastMsg!.toJson();
    }
    return map;
  }
}
