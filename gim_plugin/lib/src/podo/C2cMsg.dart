/// id : 0
/// from_uid : ""
/// to_uid : ""
/// msg_type : ""
/// msg_data : ""
/// code : 0
/// readed : false
/// err_tip : ""
/// del_uid : ""
/// created_at : 0

class C2cMsg {
  C2cMsg({
    this.id,
    this.preId,
    this.fromUid,
    this.toUid,
    this.msgType,
    this.msgData,
    this.code,
    this.readed,
    this.errTip,
    this.delUid,
    this.createdAt,
  });

  C2cMsg.fromJson(dynamic json) {
    id = json['id'];
    preId = json['pre_id'];
    fromUid = json['from_uid'];
    toUid = json['to_uid'];
    msgType = json['msg_type'];
    msgData = json['msg_data'];
    code = json['code'];
    readed = json['readed'];
    errTip = json['err_tip'];
    delUid = json['del_uid'];
    createdAt = json['created_at'];
  }

  int? id;
  String? preId;
  String? fromUid;
  String? toUid;
  String? msgType;
  String? msgData;
  int? code;
  bool? readed;
  String? errTip;
  String? delUid;
  int? createdAt;

  C2cMsg copyWith({
    int? id,
    String? preId,
    String? fromUid,
    String? toUid,
    String? msgType,
    String? msgData,
    int? code,
    bool? readed,
    String? errTip,
    String? delUid,
    int? createdAt,
  }) =>
      C2cMsg(
        id: id ?? this.id,
        preId: preId ?? this.preId,
        fromUid: fromUid ?? this.fromUid,
        toUid: toUid ?? this.toUid,
        msgType: msgType ?? this.msgType,
        msgData: msgData ?? this.msgData,
        code: code ?? this.code,
        readed: readed ?? this.readed,
        errTip: errTip ?? this.errTip,
        delUid: delUid ?? this.delUid,
        createdAt: createdAt ?? this.createdAt,
      );

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = id;
    map['pre_id'] = preId;
    map['from_uid'] = fromUid;
    map['to_uid'] = toUid;
    map['msg_type'] = msgType;
    map['msg_data'] = msgData;
    map['code'] = code;
    map['readed'] = readed;
    map['err_tip'] = errTip;
    map['del_uid'] = delUid;
    map['created_at'] = createdAt;
    return map;
  }
}
