import 'consts.dart';

const srvHost = "http://127.0.0.1:9135";
const cosHost = "https://obj.cos.com";
const gimDomain = "127.0.0.1:9135";
const gimCosHost = "https://obj.cos.com";
const artcAppID = "xxx";

String genCosUrl(String key) {
  if (!key.startsWith(ASSET_PREFIX)) return key;
  return '$cosHost/$key'.trim();
}
