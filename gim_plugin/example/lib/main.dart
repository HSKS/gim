import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:gim_plugin_example/consts.dart';
import 'package:gim_plugin_example/global.dart';
import 'package:gim_plugin_example/std.dart';
import 'package:gim_plugin_example/utils/cache.dart';
import 'package:provider/provider.dart';

import 'app_nav.dart';

Future<void> main() async {
  Future.delayed(Duration.zero).then((_) => cache.init());
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) => _MyApp();
}

class _MyApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _MyAppState();
}

class _MyAppState extends State<_MyApp> {
  final delegate = AppNav();

  @override
  void initState() {
    SystemChrome.setPreferredOrientations(
      [
        DeviceOrientation.portraitUp, // 竖屏 Portrait 模式
        DeviceOrientation.portraitDown,
      ],
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
        providers: [
          ChangeNotifierProvider(create: (_) => Global()),
        ],
        child: MaterialApp.router(
          debugShowCheckedModeBanner: false,
          locale: const Locale("zh", "CN"),
          //程序支持的语言环境配置
          supportedLocales: const [Locale("zh", "CN")],
          localizationsDelegates: const [
            GlobalMaterialLocalizations.delegate,
            GlobalCupertinoLocalizations.delegate,
            GlobalWidgetsLocalizations.delegate,
          ],
          title: APP_NAME,
          theme: ThemeData(
              fontFamily: "PingFangRegular",
              backgroundColor: bgColor,
              primaryColor: bgColor,
              iconTheme: IconThemeData(color: blackColor),
              scaffoldBackgroundColor: bgColor,
              splashColor: const Color.fromRGBO(34, 34, 34, 0.1)),
          themeMode: ThemeMode.dark,
          routeInformationParser: AppRouteParser(),
          routerDelegate: delegate,
        ));
  }
}
