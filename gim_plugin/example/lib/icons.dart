import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

SvgPicture iconReaded(double size, {Color? color}) =>
    SvgPicture.asset('icons/icon_readed.svg', width: size, height: size, color: color);

SvgPicture iconSendSuc(double size, {Color? color}) =>
    SvgPicture.asset('icons/icon_send_suc.svg', width: size, height: size, color: color);

SvgPicture iconSendErr(double size, {Color? color}) =>
    SvgPicture.asset('icons/icon_send_err.svg', width: size, height: size, color: color);

SvgPicture iconSending(double size, {Color? color}) =>
    SvgPicture.asset('icons/icon_sending.svg', width: size, height: size, color: color);

SvgPicture iconWaitReply(double size, {Color? color}) =>
    SvgPicture.asset('icons/icon_wait_reply.svg', width: size, height: size, color: color);

SvgPicture iconKeyboard(double size, {Color? color}) =>
    SvgPicture.asset('icons/icon_keyboard.svg', width: size, height: size, color: color);

SvgPicture iconMicrophone(double size, {Color? color}) =>
    SvgPicture.asset('icons/icon_microphone.svg', width: size, height: size, color: color);

SvgPicture iconEmoji(double size, {Color? color}) =>
    SvgPicture.asset('icons/icon_emoji.svg', width: size, height: size, color: color);

SvgPicture iconDeleteEmoji(double width, double height, {Color? color}) =>
    SvgPicture.asset('icons/delete_emoji.svg', width: width, height: height, color: color);

SvgPicture iconImChoosePhoto(double size, {Color? color}) =>
    SvgPicture.asset('icons/im_choose_photo.svg', width: size, height: size, color: color);

SvgPicture iconImChooseVideo(double size, {Color? color}) =>
    SvgPicture.asset('icons/im_choose_video.svg', width: size, height: size, color: color);

SvgPicture iconImRtc(double size, {Color? color}) =>
    SvgPicture.asset('icons/im_rtc.svg', width: size, height: size, color: color);

SvgPicture iconVoiceWave(double size, int level, {Color? color}) =>
    SvgPicture.asset('icons/voice_$level.svg', width: size, height: size, color: color);

SvgPicture iconVideoPause(double width, double height, {Color? color}) =>
    SvgPicture.asset('icons/icon_pause.svg', width: width, height: height, color: color);

SvgPicture iconVideoPlay(double size, {Color? color}) =>
    SvgPicture.asset('icons/icon_play.svg', width: size, height: size, color: color);


SvgPicture iconImEye({required double size, Color? color}) =>
    SvgPicture.asset('icons/im_eye.svg',
        width: size, height: size, color: color, fit: BoxFit.contain);

SvgPicture iconCalling({required double size, Color? color}) =>
    SvgPicture.asset('icons/icon_calling.svg',
        width: size, height: size, color: color, fit: BoxFit.contain);

SvgPicture iconScreenZoomIn2({required double size, Color? color}) =>
    SvgPicture.asset('icons/icon_screen_zoom_in2.svg',
        width: size, height: size, color: color, fit: BoxFit.contain);

SvgPicture iconCallVoiceResult({required double size, Color? color}) =>
    SvgPicture.asset('icons/icon_call_voice_result.svg',
        width: size, height: size, color: color, fit: BoxFit.contain);

Widget openMkf() => Container(
  width: 72,
  height: 72,
  alignment: Alignment.center,
  decoration: BoxDecoration(
      color: Colors.white, borderRadius: BorderRadius.circular(36)),
  child: Image.asset('icons/open_mkf.png', width: 40, height: 40),
);

Widget closeMkf() => Container(
  width: 72,
  height: 72,
  alignment: Alignment.center,
  decoration: BoxDecoration(
      color: const Color.fromRGBO(0, 0, 0, 0.7),
      borderRadius: BorderRadius.circular(36)),
  child: Image.asset('icons/close_mkf.png', width: 40, height: 40),
);

Widget openYsq() => Container(
  width: 72,
  height: 72,
  alignment: Alignment.center,
  decoration: BoxDecoration(
      color: Colors.white, borderRadius: BorderRadius.circular(36)),
  child: Image.asset('icons/open_ysq.png', width: 40, height: 40),
);

Widget closeYsq() => Container(
  width: 72,
  height: 72,
  alignment: Alignment.center,
  decoration: BoxDecoration(
      color: const Color.fromRGBO(0, 0, 0, 0.7),
      borderRadius: BorderRadius.circular(36)),
  child: Image.asset('icons/close_ysq.png', width: 40, height: 40),
);

Widget iconAnswer() => Container(
  width: 72,
  height: 72,
  alignment: Alignment.center,
  decoration: BoxDecoration(
      color: const Color(0xFFDE9E46),
      borderRadius: BorderRadius.circular(36)),
  child: Image.asset('icons/icon_answer.png', width: 40, height: 40),
);

Widget iconHangup() => Container(
  width: 72,
  height: 72,
  alignment: Alignment.center,
  decoration: BoxDecoration(
      color: const Color(0xFFFF4589),
      borderRadius: BorderRadius.circular(36)),
  child: Image.asset('icons/icon_hangup.png', width: 40, height: 40),
);
