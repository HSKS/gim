import 'package:agora_rtc_engine/rtc_engine.dart';
import 'package:flutter/material.dart';
import 'package:gim_plugin_example/sdk/artc_sdk.dart';
import 'package:gim_plugin_example/std.dart';
import 'package:gim_plugin_example/utils/logger_utils.dart';

import 'liver_1v1_room_overlay.dart';

class Liver1V1Store extends ChangeNotifier {
  late double _x;
  late double _y;
  late double x;
  late double y;
  late double smallViewWidth;
  late double smallViewHeight;

  void setXY(double x, double y) {
    this.x = x;
    this.y = y;
    notifyListeners();
  }

  ArtcSdk? artcSdk;
  RtcEngineEventHandler? cb;
  bool enterRoom = false;
  final bool videoCall;
  final String channelName;
  final bool master;
  final int signalingId;
  final String remoteUid;
  final String remoteNickname;
  final String remoteAvatar;

  Liver1V1Store(
      {required this.videoCall,
      required this.channelName,
      required this.master,
      required this.signalingId,
      required this.remoteUid,
      required this.remoteNickname,
      required this.remoteAvatar}) {
    if (!videoCall) {
      localVideoEnable = false;
      remoteVideoEnable = false;
      smallViewWidth = windowWidth * 0.2;
      smallViewHeight = windowWidth * 0.2;
    } else {
      localVideoEnable = true;
      remoteVideoEnable = true;
      smallViewWidth = windowWidth * 0.3;
      smallViewHeight = windowHeight * 0.26;
    }
    x = windowWidth - smallViewWidth - 10;
    y = topSafeHeight + 10;
    _x = x;
    _y = y;
    cb = RtcEngineEventHandler(joinChannelSuccess: (String channel, int uid, int elapsed) {
      loggerDebug('我加入房间：\nuid=> $uid\nelapsed=> $elapsed');
      updateLocalUserEntered();
    }, userJoined: (int uid, int elapsed) {
      loggerDebug('用户加入房间：\nuid=> $uid\nelapsed=> $elapsed');
      updateRemoteUserEntered();
    }, userOffline: (int uid, UserOfflineReason reason) {
      loggerDebug('用户离开房间：\nuid=> $uid\nreason=> $reason');
      liver1v1roomOverlay.close();
    });
  }

  void updateEnterRoom() {
    enterRoom = true;
    notifyListeners();
  }

  bool localUserEntered = false;

  void updateLocalUserEntered() {
    localUserEntered = true;
    notifyListeners();
  }

  bool remoteUserEntered = false;

  void updateRemoteUserEntered() {
    remoteUserEntered = true;
    notifyListeners();
  }

  bool localVideoEnable = true;

  void updateLocalVideoEnable(bool enable) {
    localVideoEnable = enable;
    notifyListeners();
  }

  bool localAudioEnable = true;

  void updateLocalAudioEnable(bool enable) {
    localAudioEnable = enable;
    notifyListeners();
  }

  bool remoteVideoEnable = true;

  void updateRemoteVideoEnable(bool enable) {
    remoteVideoEnable = enable;
    notifyListeners();
  }

  bool remoteAudioEnable = true;

  void updateRemoteAudioEnable(bool enable) {
    remoteAudioEnable = enable;
    notifyListeners();
  }

  bool isFrontCamera = true;

  void updateIsFrontCamera() {
    isFrontCamera = !isFrontCamera;
    notifyListeners();
  }

  bool remoteBigView = true;

  void updateRemoteBigView() {
    remoteBigView = !remoteBigView;
    notifyListeners();
  }

  bool smallScreen = false;
  void updateSmallScreen() {
    smallScreen = !smallScreen;
    if (smallScreen) {
      remoteBigView = false;
    } else {
      remoteBigView = true;
    }
    notifyListeners();
  }
}
