import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:gim_plugin_example/components/my_img.dart';
import 'package:gim_plugin_example/global.dart';
import 'package:gim_plugin_example/icons.dart';
import 'package:gim_plugin_example/sdk/gim_sdk.dart';
import 'package:gim_plugin_example/std.dart';
import 'package:provider/provider.dart';

import '../liver_1v1_room_overlay.dart';
import '../liver_1v1_store.dart';

class Holding extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _HoldingState();
}

class _HoldingState extends State<Holding> {
  Future<void> enterRoom() async {
    var store = context.read<Liver1V1Store>();
    var global = context.read<Global>();
    var uid = int.parse(global.uid!.split('_')[1]);
    var dto = await gimSdk.gim.getArtcToken(uid: uid, channelName: store.channelName);
    if (dto == null) return;
    store.artcSdk!
        .createChannel(uid: uid, rtcToken: dto.token!, channelId: store.channelName);
    gimSdk.gim.signalingAccept(signalingId: store.signalingId);
    store.updateEnterRoom();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer(builder: (BuildContext context, Liver1V1Store store, Widget? child) {
      return Material(
        color: const Color.fromRGBO(23, 26, 32, 0.9),
        child: Stack(
          children: [
            SizedBox(
              width: windowWidth,
              height: windowHeight,
              child: Column(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  const SizedBox(),
                  Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      ClipRRect(
                        borderRadius: BorderRadius.circular(15),
                        child: myImg(store.remoteAvatar, width: 80, height: 80),
                      ),
                      const SizedBox(height: 10),
                      text16(store.remoteNickname, color: Colors.white)
                    ],
                  ),
                  Container(
                      width: windowWidth,
                      padding: EdgeInsets.only(bottom: bottomSafeHeight + 50),
                      child: Row(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: store.master
                            ? [
                                CupertinoButton(
                                    child: iconHangup(),
                                    onPressed: () {
                                      var global = context.read<Global>();
                                      gimSdk.gim.signalingCancel(
                                          signalingId: store.signalingId,
                                          pushTitle: global.nickname!,
                                          pushContent: '取消通话');
                                      liver1v1roomOverlay.close();
                                    })
                              ]
                            : [
                                CupertinoButton(
                                    child: iconHangup(),
                                    onPressed: () {
                                      gimSdk.gim.signalingReject(signalingId: store.signalingId);
                                      liver1v1roomOverlay.close();
                                    }),
                                CupertinoButton(child: iconAnswer(), onPressed: enterRoom)
                              ],
                      ))
                ],
              ),
            )
          ],
        ),
      );
    });
  }
}
