import 'package:flutter/material.dart';
import 'package:gim_plugin_example/std.dart';
import 'package:provider/provider.dart';

import '../liver_1v1_store.dart';

class MoveView extends StatefulWidget {
  final Widget child;

  MoveView({required this.child});

  @override
  State<StatefulWidget> createState() => _MoveViewState();
}

class _MoveViewState extends State<MoveView> {
  double x = 0;
  double y = 0;

  @override
  Widget build(BuildContext context) {
    return Consumer(builder: (BuildContext context, Liver1V1Store store, Widget? child) {
      return Positioned(
          left: store.x,
          top: store.y,
          child: SizedBox(
            width: store.smallViewWidth,
            height: store.smallViewHeight,
            child: GestureDetector(
              onTap: () {
                if (store.smallScreen) {
                  store.updateSmallScreen();
                } else {
                  store.updateRemoteBigView();
                }
              },
              onPanUpdate: (DragUpdateDetails e) {
                if (store.x + e.delta.dx < 0) {
                  store.x = 0;
                } else if (store.x + e.delta.dx > windowWidth - store.smallViewWidth) {
                  store.x = windowWidth - store.smallViewWidth;
                } else {
                  store.x += e.delta.dx;
                }
                if (store.y + e.delta.dy < 0) {
                  store.y = 0;
                } else if (store.y + e.delta.dy > windowHeight - store.smallViewHeight) {
                  store.y = windowHeight - store.smallViewHeight;
                } else {
                  store.y += e.delta.dy;
                }
                store.setXY(store.x, store.y);
              },
              child: Container(
                width: store.smallViewWidth,
                height: store.smallViewHeight,
                alignment: Alignment.center,
                decoration: BoxDecoration(
                  border: Border.all(width: 4, color: Colors.white),
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(15),
                  boxShadow: const [
                    BoxShadow(
                      color: Color.fromRGBO(203, 196, 177, 0.35), // 阴影的颜色
                      offset: Offset(0, 4), // 阴影与容器的距离
                      blurRadius: 6.0, // 高斯的标准偏差与盒子的形状卷积。
                      spreadRadius: 0.0, // 在应用模糊之前，框应该膨胀的量。
                    ),
                  ],
                ),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(15),
                  child: widget.child,
                ),
              ),
            ),
          ));
    });
  }
}
