import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:gim_plugin_example/components/my_img.dart';
import 'package:gim_plugin_example/pages/overlay_liver/artc/views/move_view.dart';
import 'package:provider/provider.dart';

import '../../../../icons.dart';
import '../../../../std.dart';
import '../liver_1v1_room_overlay.dart';
import '../liver_1v1_store.dart';

class AudioRoom extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _AudioRoomState();
}

class _AudioRoomState extends State<AudioRoom> {
  @override
  Widget build(BuildContext context) {
    return Consumer(builder: (BuildContext context, Liver1V1Store store, Widget? child) {
      return store.smallScreen
          ? MoveView(child: iconCalling(size: 40, color: themeColor))
          : Material(
              color: const Color.fromRGBO(23, 26, 32, 0.9),
              child: Stack(
                children: [
                  SizedBox(
                    width: windowWidth,
                    height: windowHeight,
                    child: Column(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        const SizedBox(),
                        Column(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            ClipRRect(
                              borderRadius: BorderRadius.circular(15),
                              child: myImg(store.remoteAvatar, width: 80, height: 80),
                            ),
                            const SizedBox(height: 10),
                            text16(store.remoteNickname, color: Colors.white)
                          ],
                        ),
                        Container(
                            width: windowWidth,
                            padding: EdgeInsets.only(bottom: bottomSafeHeight + 50),
                            child: Row(
                                mainAxisSize: MainAxisSize.max,
                                mainAxisAlignment: MainAxisAlignment.spaceAround,
                                children: [
                                  CupertinoButton(
                                      child: iconHangup(),
                                      onPressed: () {
                                        liver1v1roomOverlay.close();
                                      })
                                ]))
                      ],
                    ),
                  ),
                  setSmallScreen()
                ],
              ));
    });
  }

  Widget setSmallScreen() {
    var store = context.read<Liver1V1Store>();
    return Positioned(
        top: topSafeHeight,
        left: 10,
        child: CupertinoButton(
          onPressed: () {
            store.updateSmallScreen();
          },
          child: iconScreenZoomIn2(size: 40),
        ));
  }
}
