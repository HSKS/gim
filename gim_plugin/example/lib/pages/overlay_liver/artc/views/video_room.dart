import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:gim_plugin_example/pages/overlay_liver/artc/views/move_view.dart';
import 'package:gim_plugin_example/sdk/artc_sdk.dart';
import 'package:gim_plugin_example/std.dart';
import 'package:provider/provider.dart';

import '../../../../../icons.dart';
import '../liver_1v1_room_overlay.dart';
import '../liver_1v1_store.dart';

class VideoRoom extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _VideoRoomState();
}

class _VideoRoomState extends State<VideoRoom> {
  late String remoteUid;
  late String channelName;
  late ArtcSdk artcSdk;

  @override
  void initState() {
    var store = context.read<Liver1V1Store>();
    remoteUid = store.remoteUid;
    channelName = store.channelName;
    artcSdk = store.artcSdk!;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Selector(
      builder: (BuildContext context, bool smallScreen, Widget? child) => smallScreen
          ? buildSmallView()
          : Material(
              color: const Color(0xFF171A20),
              child: Stack(
                children: [
                  buildBigView(),
                  setSmallScreen(),
                  buildSmallView(),
                  buildOptions(),
                ],
              ),
            ),
      selector: (BuildContext context, Liver1V1Store store) => store.smallScreen,
    );
  }

  Widget setSmallScreen() {
    var store = context.read<Liver1V1Store>();
    return Positioned(
        top: topSafeHeight,
        left: 10,
        child: CupertinoButton(
          onPressed: () {
            store.updateSmallScreen();
          },
          child: iconScreenZoomIn2(size: 40),
        ));
  }

  Widget remoteVideo() {
    return artcSdk.renderRemoteVideo(uid: int.parse(remoteUid.split("_")[1]), channelId: channelName);
  }

  Widget localVideo() {
    return artcSdk.renderLocalPreview(channelId: channelName);
  }

  Widget buildBigView() {
    return SizedBox(
      width: windowWidth,
      height: windowHeight,
      child: Selector(
        builder: (BuildContext context, bool remoteBigView, Widget? child) {
          return remoteBigView ? remoteVideo() : localVideo();
        },
        selector: (BuildContext context, Liver1V1Store store) => store.remoteBigView,
      ),
    );
  }

  Widget buildSmallView() {
    return MoveView(
        child: Selector(
      builder: (BuildContext context, bool remoteBigView, Widget? child) {
        return Material(
          child: !remoteBigView ? remoteVideo() : localVideo(),
        );
      },
      selector: (BuildContext context, Liver1V1Store store) => store.remoteBigView,
    ));
  }

  Widget buildOptions() {
    return Positioned(
        left: 0,
        bottom: 0,
        child: Container(
          width: windowWidth,
          padding: EdgeInsets.only(bottom: bottomSafeHeight + 50),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            mainAxisSize: MainAxisSize.max,
            children: [
              CupertinoButton(
                  child: iconHangup(),
                  onPressed: () async {
                    await artcSdk.delChannel();
                    liver1v1roomOverlay.close();
                  })
            ],
          ),
        ));
  }
}
