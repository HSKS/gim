import 'package:flutter/material.dart';

import 'liver_1v1.dart';

class _Liver1V1RoomOverlay {
  OverlayEntry? _entry;

  bool open(BuildContext context,
      {required bool videoCall,
        required String channelName,
        required bool master,
        required int signalingId,
        required String remoteUid,
        required String remoteNickname,
        required String remoteAvatar}) {
    if (_entry != null) return false;
    _entry = OverlayEntry(
        builder: (BuildContext context) => Liver1V1(
            videoCall: videoCall,
            channelName: channelName,
            master: master,
            signalingId: signalingId,
            remoteUid: remoteUid,
            remoteNickname: remoteNickname,
            remoteAvatar: remoteAvatar));
    Overlay.of(context)!.insert(_entry!);
    return true;
  }

  void close() {
    if (_entry != null) {
      _entry?.remove();
      _entry = null;
    }
  }
}

_Liver1V1RoomOverlay liver1v1roomOverlay = _Liver1V1RoomOverlay();
