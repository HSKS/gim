import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyrefresh/easy_refresh.dart';
import 'package:gim_plugin_example/app_nav.dart';
import 'package:gim_plugin_example/components/empty_box.dart';
import 'package:gim_plugin_example/components/my_appbar.dart';
import 'package:gim_plugin_example/components/my_img.dart';
import 'package:gim_plugin_example/pages/pg_chat_room/pg_chat_room.dart';
import 'package:gim_plugin_example/pages/tab_user_list/tab_user_list_store.dart';
import 'package:loading_animation_widget/loading_animation_widget.dart';
import 'package:provider/provider.dart';

import '../../std.dart';

class TabUserList extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _TabUserListState();
}

class _TabUserListState extends State<TabUserList> {

  @override
  void initState() {
    var store = context.read<TabUserListStore>();
    store.getList(refresh: true);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: myAppBar('用户'),
      body: Consumer(
        builder: (BuildContext context, TabUserListStore store, Widget? child) {
          return EasyRefresh.custom(
              cacheExtent: 40,
              enableControlFinishRefresh: false,
              enableControlFinishLoad: false,
              header: CustomHeader(headerBuilder: (BuildContext context,
                  RefreshMode refreshState,
                  double pulledExtent,
                  double refreshTriggerPullDistance,
                  double refreshIndicatorExtent,
                  AxisDirection axisDirection,
                  bool float,
                  Duration? completeDuration,
                  bool enableInfiniteRefresh,
                  bool success,
                  bool noMore) {
                return Center(
                  child: LoadingAnimationWidget.fallingDot(color: themeColor, size: 50),
                );
              }),
              footer: CustomFooter(footerBuilder: (BuildContext context,
                  LoadMode loadState,
                  double pulledExtent,
                  double loadTriggerPullDistance,
                  double loadIndicatorExtent,
                  AxisDirection axisDirection,
                  bool float,
                  Duration? completeDuration,
                  bool enableInfiniteLoad,
                  bool success,
                  bool noMore) {
                return Center(
                  child: LoadingAnimationWidget.prograssiveDots(color: themeColor, size: 50),
                );
              }),
              onRefresh: () async {
                store.getList(refresh: true);
              },
              onLoad: () async {
                store.getList(refresh: false);
              },
              slivers: [
                store.list.isNotEmpty
                    ? SliverList(
                        delegate: SliverChildBuilderDelegate((context, index) {
                        var item = store.list[index];
                        return CupertinoButton(
                          padding: EdgeInsets.zero,
                            child: Row(
                          children: [
                            Padding(
                                padding: EdgeInsets.only(left: pgPadding, right: 10),
                                child: ClipRRect(
                                  borderRadius: BorderRadius.circular(10),
                                  child: myImg(item.avatar!, width: 50, height: 50),
                                )),
                            Expanded(
                                child: Container(
                                  width: double.infinity,
                                  height: 50,
                                  alignment: Alignment.centerLeft,
                                  decoration: BoxDecoration(
                                      border:
                                      Border(bottom: BorderSide(width: 0.5, color: Colors.black.withOpacity(0.05)))),
                                  child: text16(item.nickname!),
                                ))
                          ],
                        ), onPressed: () {
                          AppNav.of(context).push(PgChatRoom(remoteUid: item.uid!, remoteNickname: item.nickname!, remoteAvatar: item.avatar!));
                        });
                      }, childCount: store.list.length))
                    : SliverToBoxAdapter(
                        child: EmptyBox(tip: '暂无数据'),
                      )
              ]);
        },
      ),
    );
  }
}
