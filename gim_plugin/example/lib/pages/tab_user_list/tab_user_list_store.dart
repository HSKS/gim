import 'package:flutter/material.dart';
import 'package:gim_plugin_example/service/user_podo/UserInfo.dart';
import 'package:gim_plugin_example/service/user_svc.dart';

class TabUserListStore extends ChangeNotifier {
  List<UserInfo> list = [];
  int pageSize = 20;
  int pageNo = 1;

  Future<void> getList({bool refresh = false}) async {
    if (refresh) {
      pageNo = 1;
    }
    var dto = await UserSvc.getUserList(pageNo: pageNo, pageSize: pageSize);
    if (dto == null) return;
    if (dto.arr!.isNotEmpty) {
      if (refresh) {
        list = dto.arr!;
      } else {
        list.addAll(dto.arr!);
      }
      pageNo ++;
      notifyListeners();
    } else if (refresh) {
      list = [];
      notifyListeners();
    }
  }
}