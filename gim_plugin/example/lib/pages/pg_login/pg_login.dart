import 'package:flutter/material.dart';
import 'package:gim_plugin_example/app_nav.dart';
import 'package:gim_plugin_example/components/input_box.dart';
import 'package:gim_plugin_example/components/my_appbar.dart';
import 'package:gim_plugin_example/components/my_btn.dart';
import 'package:gim_plugin_example/config.dart';
import 'package:gim_plugin_example/consts.dart';
import 'package:gim_plugin_example/global.dart';
import 'package:gim_plugin_example/pages/app_tabs/app_tabs.dart';
import 'package:gim_plugin_example/pages/pg_login/pg_complete_profile.dart';
import 'package:gim_plugin_example/sdk/gim_sdk.dart';
import 'package:gim_plugin_example/service/user_svc.dart';
import 'package:gim_plugin_example/std.dart';
import 'package:gim_plugin_example/utils/platform_utils.dart';
import 'package:gim_plugin_example/utils/toast_utils.dart';
import 'package:provider/provider.dart';

class PgLogin extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _PgLoginState();
}

class _PgLoginState extends State<PgLogin> {
  TextEditingController controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: myAppBar('登录'),
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.all(pgPadding),
          child: Column(
            children: [
              inputBox(controller: controller, placeholder: '请输入用户ID'),
              const SizedBox(height: 50),
              MyBtn(
                  color: themeColor,
                  width: windowWidth * 0.7,
                  height: 48,
                  borderRadius: 24,
                  onClick: () async {
                    if (controller.text.isEmpty) {
                      showToast('请输入用户ID');
                      return;
                    }
                    var info = await UserSvc.userLogin(uid: '${APP_ID}_${controller.text}');
                    if (info == null) return;
                    var global = context.read<Global>();
                    global.token = info.token;
                    global.uid = info.uid;
                    global.nickname = info.nickname;
                    global.avatar = info.avatar;
                    global.gender = info.gender;
                    gimSdk.init(
                        ssl: false,
                        debug: true,
                        uid: info.uid!,
                        domain: gimDomain,
                        appId: APP_ID,
                        platform: (await getPlatformInfo())!,
                        token: info.token!);
                    if (info.regStep != 100) {
                      AppNav.of(context).push(PgCompleteProfile());
                    } else {
                      AppNav.of(context).replaceAll(AppTabs());
                    }
                  },
                  child: const Text('登录', style: TextStyle(color: Colors.white)))
            ],
          ),
        ),
      ),
    );
  }
}
