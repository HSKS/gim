import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:gim_plugin_example/app_nav.dart';
import 'package:gim_plugin_example/components/input_box.dart';
import 'package:gim_plugin_example/components/ios_options.dart';
import 'package:gim_plugin_example/components/loading_modal.dart';
import 'package:gim_plugin_example/components/my_appbar.dart';
import 'package:gim_plugin_example/components/my_btn.dart';
import 'package:gim_plugin_example/components/my_img.dart';
import 'package:gim_plugin_example/global.dart';
import 'package:gim_plugin_example/pages/app_tabs/app_tabs.dart';
import 'package:gim_plugin_example/sdk/gim_sdk.dart';
import 'package:gim_plugin_example/service/user_svc.dart';
import 'package:gim_plugin_example/std.dart';
import 'package:gim_plugin_example/utils/permission_utils.dart';
import 'package:gim_plugin_example/utils/toast_utils.dart';
import 'package:image_picker/image_picker.dart';
import 'package:provider/src/provider.dart';

class PgCompleteProfile extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _PgCompleteProfileStore();
}

class _PgCompleteProfileStore extends State<PgCompleteProfile> {
  TextEditingController nicknameCtrl = TextEditingController();

  int gender = 0;
  String? avatar;

  Widget buildGender() {
    return CupertinoButton(
        padding: EdgeInsets.zero,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            text20(
                gender == 0
                    ? '性别'
                    : gender == 1
                        ? '男'
                        : '女',
                color: gender == 0 ? Colors.grey : Colors.black),
            const SizedBox(height: 10),
            Container(
              width: double.infinity,
              height: 1.2,
              color: Colors.grey,
            ),
          ],
        ),
        onPressed: () {
          IosOptions.showBottomOptions(context, items: [
            IosOptionItem(
                label: '男',
                onTap: () {
                  setState(() {
                    gender = 1;
                  });
                }),
            IosOptionItem(
                label: '女',
                onTap: () {
                  setState(() {
                    gender = 2;
                  });
                }),
          ]);
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: myAppBar('完善资料'),
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.all(pgPadding),
          child: Column(
            children: [
              CupertinoButton(
                  child: Container(
                    width: 140,
                    height: 140,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20),
                        border: Border.all(color: const Color.fromRGBO(24, 24, 24, 0.1))),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(20),
                      child: myImg(avatar ?? 'images/default_avatar.png', width: 140, height: 140),
                    ),
                  ),
                  onPressed: () async {
                    if (!await applyPhotosPermission(context: context)) {
                      return;
                    }
                    final XFile? image = await ImagePicker().pickImage(source: ImageSource.gallery);
                    if (image == null) return;
                    setState(() {
                      avatar = image.path;
                    });
                  }),
              const SizedBox(height: 50),
              inputBox(controller: nicknameCtrl, placeholder: '请输入昵称'),
              const SizedBox(height: 50),
              buildGender(),
              const SizedBox(height: 50),
              MyBtn(
                  color: themeColor,
                  width: windowWidth * 0.7,
                  height: 48,
                  borderRadius: 24,
                  onClick: () async {
                    if (nicknameCtrl.text.isEmpty) {
                      showToast('请输入昵称');
                      return;
                    }
                    if (gender == 0) {
                      showToast('请选择性别');
                      return;
                    }
                    if (avatar == null) {
                      showToast('请选择头像');
                      return;
                    }
                    try {
                      loadingModal.open(context);
                      var key = await gimSdk.gim.uploadFile(filePath: avatar!);
                      if (key == null) {
                        showToast('头像上传失败');
                        return;
                      }
                      var user = await UserSvc.editProfile(
                          nickname: nicknameCtrl.text, avatar: key, gender: gender, regStep: 100);
                      if (user == null) {
                        showToast('完善资料失败');
                        return;
                      }
                      var global = context.read<Global>();
                      global.nickname = user.nickname;
                      global.avatar = user.avatar;
                      global.gender = user.gender;
                      AppNav.of(context).replaceAll(AppTabs());
                    } finally {
                      loadingModal.close();
                    }
                  },
                  child: const Text('完成', style: TextStyle(color: Colors.white)))
            ],
          ),
        ),
      ),
    );
  }
}
