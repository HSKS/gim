import 'dart:convert';
import 'dart:math' as math;
import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/material.dart';
import 'package:gim_plugin/gim_plugin.dart';
import 'package:gim_plugin_example/consts.dart';
import 'package:gim_plugin_example/utils/cache.dart';


import '../../../config.dart';
import '../../../icons.dart';
import '../../../std.dart';
import 'msg_box.dart';

class VoiceMsg extends StatefulWidget {
  final C2cMsg msg;

  VoiceMsg({Key? key, required this.msg}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _VoiceMsgState();
}

class _VoiceMsgState extends State<VoiceMsg> {
  final AudioPlayer player = AudioPlayer();

  late AudioMsg audioMsg;
  int duration = 0;

  int level = 0;

  int shan = 0;

  @override
  void initState() {
    audioMsg = AudioMsg.fromJson(json.decode(widget.msg.msgData!));
    duration = (audioMsg.dur! ~/ 1000);
    init();
    super.initState();
  }

  Future<void> init() async {
    int totalDur = audioMsg.dur! ~/ 1000;
    player.onAudioPositionChanged.listen((event) {
      if (event.inSeconds > totalDur) {
        setState(() {
          shan = 0;
          level = 0;
          duration = totalDur;
        });
      } else {
        if ((event.inMilliseconds - shan) > 300) {
          shan = event.inMilliseconds;
          level = (level + 1) % 3;
        }
        setState(() {
          duration = event.inSeconds;
        });
      }
    });
    player.onPlayerCompletion.listen((event) {
      setState(() {
        shan = 0;
        level = 0;
        duration = totalDur;
      });
    });
  }

  @override
  void dispose() {
    player.stop();
    player.dispose();
    super.dispose();
  }

  Future<void> play() async {
    if (player.state == PlayerState.PLAYING) {
      shan = 0;
      level = 0;
      duration = audioMsg.dur! ~/ 1000;
      player.stop();
    } else {
      shan = 0;
      level = 0;
      duration = 0;
      player.play(genCosUrl(audioMsg.url!));
    }
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return MsgBox(
        msg: widget.msg,
        onTap: play,
        child: Text.rich(TextSpan(
            children: widget.msg.fromUid == cache.getStringSync(UID)
                ? [
              TextSpan(text: "$duration''", style: TextStyle(color: Colors.white, fontSize: font16)),
              WidgetSpan(
                  child: Padding(
                    padding: const EdgeInsets.only(left: 15),
                    child: Transform.rotate(
                        angle: math.pi, child: iconVoiceWave(font16, 2 - level, color: Colors.white)),
                  ))
            ]
                : [
              WidgetSpan(
                  child: Padding(
                    padding: const EdgeInsets.only(right: 15),
                    child: iconVoiceWave(font16, 2 - level, color: blackColor),
                  )),
              TextSpan(text: "$duration''", style: TextStyle(color: blackColor, fontSize: font16)),
            ])));
  }
}
