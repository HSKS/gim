import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:gim_plugin/gim_plugin.dart';
import 'package:gim_plugin_example/components/ios_options.dart';
import 'package:gim_plugin_example/components/my_img.dart';
import 'package:gim_plugin_example/global.dart';
import 'package:gim_plugin_example/utils/data_utils.dart';
import 'package:gim_plugin_example/utils/date_utils.dart';
import 'package:loading_animation_widget/loading_animation_widget.dart';
import 'package:provider/provider.dart';

import '../../../icons.dart';
import '../../../std.dart';
import '../pg_chat_room_store.dart';

class MsgBox extends StatefulWidget {

  final VoidCallback? onTap;

  final C2cMsg msg;

  final Widget child;

  final Color? bgColor;

  final double padding;

  MsgBox({Key? key, required this.msg, this.onTap, required this.child, this.bgColor, this.padding = 10})
      : super(key: key);
  
  @override
  State<StatefulWidget> createState() => _MsgBoxState();
  
}

class _MsgBoxState extends State<MsgBox> {
  
  late bool myself;

  late Color bgColor;

  late String avatar;
  late String nickname;
  
  @override
  void initState() {
    var global = context.read<Global>();
    myself = widget.msg.fromUid == global.uid;
    if (myself) {
      avatar = global.avatar!;
      nickname = global.nickname!;
      if (widget.bgColor != null) {
        bgColor = widget.bgColor!;
      } else {
        bgColor = themeColor;
      }
    } else {
      var store = context.read<PgChatRoomStore>();
      avatar = store.remoteAvatar;
      nickname = store.remoteNickname;
      if (widget.bgColor != null) {
        bgColor = widget.bgColor!;
      } else {
        bgColor = Colors.white;
      }
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    TextDirection textDirection = myself ? TextDirection.rtl : TextDirection.ltr;
    CrossAxisAlignment crossAxisAlignment = myself ? CrossAxisAlignment.end : CrossAxisAlignment.start;
    List<IosOptionItem> iosOptionItems = [
      IosOptionItem(
          label: '删除',
          onTap: () async {

          })
    ];
    if (myself) {
      iosOptionItems.add(IosOptionItem(
          label: '撤回',
          onTap: () async {

          }));
    }
    return Container(
      width: double.infinity,
      padding: EdgeInsets.only(left: pgPadding, right: pgPadding, top: 12, bottom: 12),
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.only(bottom: 12),
            child: text12(diffCurrent(DateTime.fromMillisecondsSinceEpoch(widget.msg.createdAt! * 1000)),
                color: greyColor),
          ),
          Row(
            mainAxisSize: MainAxisSize.max,
            textDirection: textDirection,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              MaterialButton(
                minWidth: 40,
                height: 40,
                elevation: 0,
                hoverElevation: 0,
                highlightElevation: 0,
                splashColor: const Color.fromRGBO(34, 34, 34, 0.1),
                padding: EdgeInsets.zero,
                shape: const CircleBorder(),
                materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                onPressed: () async {

                },
                child: ClipRRect(
                  borderRadius: myself?const BorderRadius.only(topLeft: Radius.circular(20), bottomLeft: Radius.circular(20),bottomRight: Radius.circular(20)):const BorderRadius.only(topRight: Radius.circular(20), bottomLeft: Radius.circular(20),bottomRight: Radius.circular(20)),
                  child: myImg(avatar, width: 40, height: 40),
                ),
              ),
              const SizedBox(width: 8),
              Expanded(
                  child: Column(
                crossAxisAlignment: crossAxisAlignment,
                mainAxisSize: MainAxisSize.min,
                children: [
                  // Offstage(
                  //   offstage: myself,
                  //   child: Padding(
                  //     padding: const EdgeInsets.only(bottom: 5),
                  //     child: std.text10(msg.nickname!, color: std.grayColor),
                  //   ),
                  // ),
                  Row(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    textDirection: textDirection,
                    children: [
                      MaterialButton(
                        minWidth: 50,
                        onPressed: widget.onTap,
                        onLongPress: () {
                          IosOptions.showBottomOptions(context, items: iosOptionItems);
                        },
                        elevation: 0,
                        hoverElevation: 0,
                        highlightElevation: 0,
                        padding: const EdgeInsets.all(0),
                        splashColor: const Color.fromRGBO(34, 34, 34, 0.1),
                        materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                        color: bgColor,
                        shape: RoundedRectangleBorder(
                            borderRadius: myself
                                ? const BorderRadius.only(
                                    topLeft: Radius.circular(10),
                                    bottomLeft: Radius.circular(10),
                                    bottomRight: Radius.circular(10))
                                : const BorderRadius.only(
                                    topRight: Radius.circular(10),
                                    bottomLeft: Radius.circular(10),
                                    bottomRight: Radius.circular(10))),
                        child: Container(
                          padding: EdgeInsets.all(widget.padding),
                          constraints: BoxConstraints(maxWidth: windowWidth - 136),
                          child: widget.child,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 4, right: 4, bottom: 12),
                        child: widget.msg.code == 0
                            ? (myself
                                ? widget.msg.readed!
                                    ? iconReaded(16)
                                    : iconSendSuc(16)
                                : const SizedBox(
                                    width: 16,
                                    height: 16,
                                  ))
                            : SizedBox(
                                width: 16,
                                height: 16,
                                child: Offstage(
                                  offstage: !myself,
                                  child: widget.msg.code == null
                                      ? LoadingAnimationWidget.hexagonDots(color: themeColor, size: 12)
                                      : CupertinoButton(
                                          padding: EdgeInsets.zero,
                                          borderRadius: BorderRadius.circular(8),
                                          onPressed: () async {

                                          },
                                          child: iconSendErr(16)),
                                ),
                              ),
                      )
                    ],
                  )
                ],
              ))
            ],
          ),
          Offstage(
            offstage: isBlack(widget.msg.errTip),
            child: Container(
              padding: const EdgeInsets.all(10),
              margin: const EdgeInsets.only(top: 10),
              decoration: BoxDecoration(
                color: Colors.black.withOpacity(0.03),
                borderRadius: BorderRadius.circular(8)
              ),
              child: text12(widget.msg.errTip ?? '', color: greyColor),
            ),
          )
        ],
      ),
    );
  }
}
