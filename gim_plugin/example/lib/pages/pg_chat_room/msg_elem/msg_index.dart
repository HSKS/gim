import 'package:flutter/material.dart';
import 'package:gim_plugin/gim_plugin.dart';
import 'package:gim_plugin_example/pages/pg_chat_room/msg_elem/img_msg.dart';
import 'package:gim_plugin_example/pages/pg_chat_room/msg_elem/text_msg.dart';
import 'package:gim_plugin_example/pages/pg_chat_room/msg_elem/video_msg.dart';
import 'package:gim_plugin_example/pages/pg_chat_room/msg_elem/voice_msg.dart';

class MsgIndex extends StatefulWidget {
  final C2cMsg msg;

  MsgIndex({Key? key, required this.msg}) : super(key: key);
  @override
  State<StatefulWidget> createState() => _MsgIndexState();

}

class _MsgIndexState extends State<MsgIndex> {


  @override
  Widget build(BuildContext context) {
    switch (widget.msg.msgType!) {
      case MsgTypes.MsgTypeText:
        return TextMsg(msg: widget.msg);
      case MsgTypes.MsgTypeImage:
        return ImgMsg(msg: widget.msg);
      case MsgTypes.MsgTypeAudio:
        return VoiceMsg(msg: widget.msg);
      case MsgTypes.MsgTypeVideo:
        return VdoMsg(msg: widget.msg);
      default:
        return const SizedBox();
    }
  }
}
