import 'package:flutter/material.dart';
import 'package:gim_plugin/gim_plugin.dart';
import 'package:gim_plugin_example/consts.dart';
import 'package:gim_plugin_example/utils/cache.dart';

import '../../../std.dart';
import 'msg_box.dart';

class TextMsg extends StatelessWidget {
  final C2cMsg msg;

  TextMsg({Key? key, required this.msg}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MsgBox(
        msg: msg,
        child: text16(msg.msgData ?? '', color: cache.getStringSync(UID) == msg.fromUid ? Colors.white : blackColor));
  }
}
