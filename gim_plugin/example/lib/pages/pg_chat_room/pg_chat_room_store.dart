import 'package:flutter/material.dart';
import 'package:gim_plugin/gim_plugin.dart';
import 'package:gim_plugin_example/sdk/gim_sdk.dart';

class PgChatRoomStore extends ChangeNotifier {
  final FocusNode focusNode = FocusNode();
  final GlobalKey listGlobalKey = GlobalKey();
  final TextEditingController textController = TextEditingController();
  final String remoteUid;
  final String remoteNickname;
  final String remoteAvatar;
  bool showEmoji = false;
  int notifyInput = 0;
  int notifyData = 0;
  bool isFocus = false;

  void updateShowEmoji({bool? flag}) {
    focusNode.unfocus();
    if (flag != null) {
      showEmoji = flag;
    } else {
      showEmoji = !showEmoji;
    }
    notifyListeners();
  }

  void updateNotifyInput() {
    notifyInput = textController.text.length;
    notifyListeners();
  }
  int lastTs = DateTime.now().millisecondsSinceEpoch ~/ 1000;



  PgChatRoomStore({required this.remoteUid, required this.remoteNickname, required this.remoteAvatar}){
    focusNode.addListener(focusNodeListener);
    getMsgList(refresh: true);
  }

  void focusNodeListener() {
    isFocus = focusNode.hasFocus;
    notifyListeners();
  }

  @override
  void dispose() {
    focusNode.removeListener(focusNodeListener);
    focusNode.dispose();
    textController.dispose();
    super.dispose();
  }

  void addMsg(C2cMsg msg) {
    msgList.add(msg);
    notifyData = DateTime.now().microsecondsSinceEpoch;
    notifyListeners();
  }

  void updateMsgByPreId(C2cMsg msg) {
    for (int i = 0;i < msgList.length;i++) {
      if (msg.preId == msgList[i].preId) {
        msgList[i] = msg;
        notifyData = DateTime.now().microsecondsSinceEpoch;
        notifyListeners();
        break;
      }
    }
  }

  void editMsg(C2cMsg msg) {
    for (int i = 0;i < msgList.length;i++) {
      if (msg.id == msgList[i].id) {
        msgList[i] = msg;
        notifyData = DateTime.now().microsecondsSinceEpoch;
        notifyListeners();
        break;
      }
    }
  }

  void markMsgAsRead() {
    for(int i = 0;i < msgList.length;i++) {
      msgList[i].readed = true;
    }
    notifyData = DateTime.now().microsecondsSinceEpoch;
    notifyListeners();
  }

  void delMsg(int msgId) {
    for (int i = 0;i < msgList.length;i++) {
      if (msgId == msgList[i].id) {
        msgList.removeAt(i);
        notifyData = DateTime.now().microsecondsSinceEpoch;
        notifyListeners();
        break;
      }
    }
  }

  List<C2cMsg> msgList = [];
  int? lastMsgID;
  Future<void> getMsgList({bool refresh = false}) async {
    if (refresh) {
      lastMsgID = null;
    }
    var dto = await gimSdk.gim.getC2cMsgList(toUid: remoteUid, lastId: lastMsgID);
    if (dto == null) return;
    lastMsgID = dto.lastId;
    notifyData = DateTime.now().microsecondsSinceEpoch;
    if (dto.arr!.isNotEmpty) {

      if (refresh) {
        msgList = dto.arr!.reversed.toList();
      } else {
        msgList.addAll(dto.arr!.reversed.toList());
      }
      notifyListeners();
    } else if (refresh) {
      msgList = [];
      notifyListeners();
    }
  }
}