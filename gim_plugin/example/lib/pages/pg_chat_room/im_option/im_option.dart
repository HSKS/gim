import 'package:flutter/material.dart';
import 'package:gim_plugin_example/pages/pg_chat_room/im_option/sendmsg_layout.dart';

import 'package:provider/provider.dart';

import '../../../std.dart';
import '../pg_chat_room_store.dart';
import 'bottom_layout.dart';
import 'emoji_layout.dart';

class ImOption extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _ImOptionState();
}

class _ImOptionState extends State<ImOption> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: windowWidth,
      padding: EdgeInsets.only(top: pgPadding),
      decoration: const BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(topLeft: Radius.circular(20), topRight: Radius.circular(20))),
      child: Selector(
        builder: (BuildContext _, bool showEmoji, Widget? child) => Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            SendmsgLayout(),
            Selector(
                builder: (BuildContext _, bool isFocus, Widget? child) =>
                isFocus ? const SizedBox(height: 20) : (showEmoji ? EmojiLayout() : BottomLayout()),
                selector: (BuildContext _, PgChatRoomStore store) => store.isFocus)
          ],
        ),
        selector: (BuildContext _, PgChatRoomStore store) => store.showEmoji,
      ),
    );
  }
}
