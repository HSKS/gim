import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:gim_plugin_example/sdk/gim_sdk.dart';
import 'package:gim_plugin_example/utils/data_utils.dart';
import 'package:provider/provider.dart';
import '../../../icons.dart';
import '../../../std.dart';
import '../pg_chat_room_store.dart';

class SendText extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _SendTextState();
}

class _SendTextState extends State<SendText> {
  Future<void> sendMsg() async {
    PgChatRoomStore store = context.read<PgChatRoomStore>();
    if (isBlack(store.textController.text)) return;
    String text = store.textController.text;
    store.textController.text = '';
    store.focusNode.unfocus();
    store.updateShowEmoji(flag: false);
    store.updateNotifyInput();
    var dto = await gimSdk.gim.sendTextMsg(toUid: store.remoteUid, text: text, placeholder: (msg) {
      store.addMsg(msg);
    });
    if (dto == null) return;
    store.updateMsgByPreId(dto);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      padding: const EdgeInsets.all(6),
      decoration: BoxDecoration(color: const Color.fromRGBO(34, 34, 34, 0.05), borderRadius: BorderRadius.circular(20)),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          Expanded(
              child: Selector(
            builder: (BuildContext _, TextEditingController textController, Widget? child) => Selector(
              builder: (BuildContext _, FocusNode focusNode, Widget? child) => CupertinoTextField(
                focusNode: focusNode,
                placeholder: '请输入文字',
                placeholderStyle: const TextStyle(fontSize: 18, color: Color.fromRGBO(34, 34, 34, 0.4)),
                maxLength: 100,
                minLines: 1,
                maxLines: 4,
                cursorHeight: 20,
                padding: const EdgeInsets.only(left: 14, right: 5, top: 2, bottom: 2),
                controller: textController,
                onChanged: (s) {
                  PgChatRoomStore store = context.read<PgChatRoomStore>();
                  store.updateNotifyInput();
                },
                onSubmitted: (s) {
                  sendMsg();
                },
                textInputAction: TextInputAction.send,
                autocorrect: false,
                autofocus: false,
                textAlign: TextAlign.left,
                keyboardType: TextInputType.text,
                cursorColor: themeColor,
                style: TextStyle(fontSize: 18, color: blackColor),
                decoration: const BoxDecoration(color: Colors.transparent),
              ),
              selector: (BuildContext _, PgChatRoomStore store) => store.focusNode,
            ),
            selector: (BuildContext _, PgChatRoomStore store) => store.textController,
          )),
          MaterialButton(
            minWidth: 28,
            height: 28,
            elevation: 0,
            hoverElevation: 0,
            highlightElevation: 0,
            splashColor: const Color.fromRGBO(34, 34, 34, 0.1),
            padding: EdgeInsets.zero,
            shape: const CircleBorder(),
            materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
            onPressed: () {
              PgChatRoomStore store = context.read<PgChatRoomStore>();
              store.updateShowEmoji(flag: true);
            },
            child: iconEmoji(28),
          ),
          Offstage(
            offstage: Platform.isIOS,
            child: Padding(
              padding: const EdgeInsets.only(left: 5),
              child: SizedBox(
                height: 28,
                child: Selector(
                  builder: (BuildContext _, int val, Widget? child) => CupertinoButton(
                    color: val > 0 ? themeColor : const Color(0xFFCCCCCC),
                    padding: EdgeInsets.zero,
                    borderRadius: BorderRadius.circular(14),
                    onPressed: () {
                      sendMsg();
                    },
                    child: text12('发送', color: Colors.white),
                  ),
                  selector: (BuildContext _, PgChatRoomStore store) => store.notifyInput,
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
