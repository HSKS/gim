import 'package:flutter/cupertino.dart';
import 'package:provider/provider.dart';

import '../../../icons.dart';
import '../../../std.dart';
import '../pg_chat_room_store.dart';

class EmojiLayout extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: windowWidth,
      height: 300,
      padding: const EdgeInsets.only(top: 20, left: 4, right: 4),
      // color: Colors.blue,
      alignment: Alignment.center,
      child: Selector(
        builder: (BuildContext _, int val, Widget? child) => Stack(
          children: [
            GridView.count(
              crossAxisCount: 6,
              children: emojis
                  .map((e) => CupertinoButton(
                        padding: EdgeInsets.zero,
                        onPressed: () {
                          PgChatRoomStore store = context.read<PgChatRoomStore>();
                          store.textController.text += e;
                          store.updateNotifyInput();
                        },
                        child: Text(e, style: const TextStyle(fontSize: 32)),
                      ))
                  .toList(),
            ),
            Positioned(
                bottom: 10,
                right: 20,
                child: Offstage(
                  offstage: val == 0,
                  child: CupertinoButton(
                    padding: EdgeInsets.zero,
                    onPressed: () {
                      PgChatRoomStore store = context.read<PgChatRoomStore>();
                      var txt = store.textController.text.characters.skipLast(1);
                      store.textController.text = txt.toString();
                      store.updateNotifyInput();
                    },
                    child: iconDeleteEmoji(64, 40),
                  ),
                ))
          ],
        ),
        selector: (BuildContext _, PgChatRoomStore store) => store.notifyInput,
      ),
    );
  }
}

List<String> emojis = [
  "😀",
  "😃",
  "😄",
  "😁",
  "😆",
  "😅",
  "😂",
  "🤣",
  "️😊",
  "😇",
  "🙂",
  "🙃",
  "😉",
  "😌",
  "😍",
  "🥰",
  "😘",
  "😗",
  "😙",
  "😚",
  "😋",
  "😛",
  "😝",
  "😜",
  "🤪",
  "🤨",
  "🧐",
  "🤓",
  "😎",
  "🤩",
  "🥳",
  "😏",
  "😒",
  "😞",
  "😔",
  "😟",
  "😕",
  "🙁",
  "️😣",
  "😖",
  "😫",
  "😩",
  "🥺",
  "😢",
  "😭",
  "😤",
  "😠",
  "😡",
  "🤬",
  "🤯",
  "😳",
  "🥵",
  "🥶",
  "😶",
  "😱",
  "😨",
  "😰",
  "😥",
  "😓",
  "🤗",
  "🤔",
  "🤭",
  "🤭",
  "🤫",
  "🤥",
  "😶",
  "😐",
  "😑",
  "😬",
  "🙄",
  "😯",
  "😦",
  "😧",
  "😮",
  "😲",
  "🥱",
  "😴",
  "🤤",
  "😪",
  "😮‍💨",
  "😵",
  "😵‍💫",
  "🤐",
  "🥴",
  "🤢",
  "🤮",
  "🤧",
  "😷",
  "🤒",
  "🤕",
  "🤑",
  "🤠",
  "😈",
  "👿",
  "👹",
  "👺",
  "🤡",
  "💩",
  "👻",
  "💀",
  "👽",
  "👾",
  "🤖",
  "🎃",
  "😺",
  "😸",
  "😹",
  "😻",
  "😼",
  "😽",
  "🙀",
  "😿",
  "😾",
  "💋",
  "👄",
  "👅"
];

