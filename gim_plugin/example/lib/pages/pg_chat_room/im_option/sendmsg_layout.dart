import 'package:flutter/material.dart';
import 'package:gim_plugin_example/pages/pg_chat_room/im_option/send_text.dart';
import 'package:gim_plugin_example/pages/pg_chat_room/im_option/send_voice.dart';
import 'package:gim_plugin_example/utils/permission_utils.dart';

import '../../../icons.dart';
import '../../../std.dart';

class SendmsgLayout extends StatefulWidget {

  @override
  State<StatefulWidget> createState() => _SendmsgLayoutState();
}

class _SendmsgLayoutState extends State<SendmsgLayout> {
  bool sendText = true;

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: EdgeInsets.only(left: pgPadding, right: pgPadding),
        child: Row(
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            MaterialButton(
              minWidth: 40,
              height: 40,
              elevation: 0,
              hoverElevation: 0,
              highlightElevation: 0,
              splashColor: const Color.fromRGBO(34, 34, 34, 0.1),
              padding: EdgeInsets.zero,
              shape: const CircleBorder(),
              materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
              onPressed: () async {
                if (!sendText) {
                  if (!await applyMicrophonePermission(context:context)) {
                    return;
                  }
                }
                setState(() {
                  sendText = !sendText;
                });
              },
              child: sendText ? iconMicrophone(40) : iconKeyboard(40),
            ),
            const SizedBox(width: 10),
            Expanded(
                child: sendText
                    ? SendText()
                    : SendVoice())
          ],
        ));
  }
}
