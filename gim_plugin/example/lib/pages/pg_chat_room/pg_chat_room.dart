import 'package:flutter/material.dart';
import 'package:gim_plugin/gim_plugin.dart';
import 'package:gim_plugin_example/components/my_appbar.dart';
import 'package:gim_plugin_example/pages/pg_chat_room/pg_chat_room_store.dart';
import 'package:gim_plugin_example/sdk/gim_sdk.dart';
import 'package:provider/provider.dart';

import 'im_option/im_option.dart';
import 'msg_elem/msg_index.dart';

class PgChatRoom extends StatelessWidget {
  VoidCallback? onback;
  final String remoteUid;
  final String remoteNickname;
  final String remoteAvatar;

  PgChatRoom({required this.remoteUid, required this.remoteNickname, required this.remoteAvatar, this.onback});

  @override
  Widget build(BuildContext context) {
    return MultiProvider(providers: [
      ChangeNotifierProvider(create: (_) => PgChatRoomStore(remoteUid: remoteUid, remoteNickname: remoteNickname, remoteAvatar: remoteAvatar)),
    ],
      child: _PgChatRoom(onback: onback),
    );
  }
}

class _PgChatRoom extends StatefulWidget {
  VoidCallback? onback;

  _PgChatRoom({this.onback});

  @override
  State<StatefulWidget> createState() => _PgChatRoomStore();

}

class _PgChatRoomStore extends State<_PgChatRoom> {
  final ScrollController scrollController = ScrollController(initialScrollOffset: 0);
  late String remoteNickname;

  @override
  void initState() {
    var store = context.read<PgChatRoomStore>();
    remoteNickname = store.remoteNickname;

    //添加收到新消息通知
    gimSdk.gim.addNewC2cMsgFn(onNewMsg);
    //添加消息修改通知
    gimSdk.gim.addEditC2cMsgFn(onEditMsg);
    //添加消息已读通知
    gimSdk.gim.addMarkMsgAsReadFn(onMsgReaded);
    //添加消息撤回通知
    gimSdk.gim.addWithdrawC2cMsgFn(onMsgWithdraw);

    Future.delayed(Duration.zero).then((_) {
      store.getMsgList(refresh: true);
      gimSdk.gim.markC2cMsgRead(fromUid: store.remoteUid);
    });
    super.initState();
  }

  void onNewMsg(C2cMsg msg) {
    var store = context.read<PgChatRoomStore>();
    store.addMsg(msg);
    Future.delayed(const Duration(milliseconds: 100)).then((_) => gimSdk.gim.markC2cMsgRead(fromUid: store.remoteUid));
  }

  void onEditMsg(C2cMsg msg) {
    var store = context.read<PgChatRoomStore>();

  }

  void onMsgReaded(String uid) {
    var store = context.read<PgChatRoomStore>();
    if (uid == store.remoteUid) {
      store.markMsgAsRead();
    }
  }

  void onMsgWithdraw(int msgID) {

  }

  @override
  void dispose() {
    scrollController.dispose();
    gimSdk.gim.delNewC2cMsgFn(onNewMsg);
    gimSdk.gim.delEditC2cMsgFn(onEditMsg);
    gimSdk.gim.delMarkMsgAsReadFn(onMsgReaded);
    gimSdk.gim.delWithdrawC2cMsgFn(onMsgWithdraw);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: myAppBar(remoteNickname),
      body: Column(
        children: [
          Expanded(child: GestureDetector(
            behavior: HitTestBehavior.opaque,
            onTap: () {
              PgChatRoomStore store = context.read<PgChatRoomStore>();
              store.focusNode.unfocus();
              store.updateShowEmoji(flag: false);
            },
            child: Align(
              alignment: Alignment.topCenter,
              child: SingleChildScrollView(
                controller: scrollController,
                reverse: true,
                child: Selector(
                  builder: (BuildContext _, int data, Widget? child) {
                    PgChatRoomStore store = context.read<PgChatRoomStore>();
                    return Column(
                      children: store.msgList.map((msg) => MsgIndex(msg: msg, key: Key('${msg.id}'))).toList(),
                    );
                  },
                  selector: (BuildContext _, PgChatRoomStore store) => store.notifyData,
                ),
              ),
            ),
          )),
          ImOption()
        ],
      ),
    );
  }

}