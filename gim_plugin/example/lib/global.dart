import 'package:flutter/material.dart';

class Global extends ChangeNotifier {
  String? uid;
  String? nickname;
  String? avatar;
  int? gender;
  String? token;
}