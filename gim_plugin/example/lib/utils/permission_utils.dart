import 'package:flutter/cupertino.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:flutter/material.dart';


Future<bool> applyPushPermission({BuildContext? context}) async {
  Permission permission = Permission.notification;
  return await _applyPermission(context, permission,
      type: "消息通知权限未授权", tip: "App需要您的同意，才能可以通过消息推送，及时将重要信息通知到您。");
}

Future<bool> applyLocationPermission({BuildContext? context}) async {
  Permission permission = Permission.locationWhenInUse;
  return await _applyPermission(context, permission,
      type: "地理位置权限未授权", tip: "App需要您的同意，才能访问位置信息，以便于发现附近的人，或者通过地理位置筛选不同区域的人。");
}

Future<bool> applyPhonePermission({BuildContext? context}) async {
  Permission permission = Permission.phone;
  return await _applyPermission(context, permission, type: "手机信息", tip: "App需要您的同意，才能访问您的手机号，以便便捷登录。");
}

Future<bool> applyCameraPermission({BuildContext? context}) async {
  Permission permission = Permission.camera;
  return await _applyPermission(context, permission, type: "摄像头权限未打开", tip: "APP需要您的同意，才能使用摄像头，以便于相机拍摄，上传、发布照片。");
}

Future<bool> applyMicrophonePermission({BuildContext? context}) async {
  Permission permission = Permission.microphone;
  return await _applyPermission(context, permission, type: "麦克风权限未打开", tip: "APP需要您的同意，才能使用麦克风，以便于发语音消息。");
}

Future<bool> applyStoragePermission({BuildContext? context}) async {
  Permission permission = Permission.manageExternalStorage;
  return await _applyPermission(context, permission, type: "设备存储权限未打开", tip: "APP需要您的同意，才能存储功能，将文件保存到您本地。");
}

Future<bool> applyPhotosPermission({BuildContext? context}) async {
  Permission permission = Permission.photos;
  return await _applyPermission(context, permission, type: "相册权限未打开", tip: "APP需要您的同意，才能访问相册，以便于图片选取、上传、发布。");
}

Future<bool> _applyPermission(BuildContext? context, Permission permission, {required type, required String tip}) async {
  if (await permission.status.isGranted) return true;
  final status = await permission.request();
  bool flag = true;
  if (status == PermissionStatus.granted) {
    flag = true;
  } else if (status == PermissionStatus.denied) {
    flag = false;
  } else if (status == PermissionStatus.permanentlyDenied) {
    flag = false;
  } else {
    flag = false;
  }
  if (!flag && context != null) {
    showCupertinoDialog(context: context, builder: (_) {
      return CupertinoAlertDialog(
        title: type,
        content: Text(tip),
        actions: [ CupertinoButton(
            child: const Text("取消", style: TextStyle(color: Colors.red)),
            onPressed: () {
              Navigator.of(context).pop();
            }),
          CupertinoButton(
              child: const Text('去开启', style: TextStyle(color: Colors.blue)),
              onPressed: () {
                Navigator.of(context).pop();
                openPermissionSettingPage();
              })
        ],
      );
    });
  }
  return flag;
}

void openPermissionSettingPage() {
  openAppSettings();
}
