import 'package:flutter/foundation.dart';
import 'package:logger/logger.dart';

var _logger = Logger(
  printer: PrettyPrinter(),
);

loggerDebug(dynamic message, [dynamic error, StackTrace? stackTrace]) {
  if (kReleaseMode) return;
  _logger.d(message, error = error, stackTrace = stackTrace);
}

loggerInfo(dynamic message, [dynamic error, StackTrace? stackTrace]) {
  if (kReleaseMode) return;
  _logger.i(message, error = error, stackTrace = stackTrace);
}

loggerWarning(dynamic message, [dynamic error, StackTrace? stackTrace]) {
  if (kReleaseMode) return;
  _logger.w(message, error = error, stackTrace = stackTrace);
}

loggerError(dynamic message, [dynamic error, StackTrace? stackTrace]) {
  if (kReleaseMode) return;
  _logger.e(message, error = error, stackTrace = stackTrace);
}
