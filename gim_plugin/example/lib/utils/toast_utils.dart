import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart' as ftoast;

int _ts = 0;
void showToast(String msg) {
  if (DateTime.now().millisecondsSinceEpoch - _ts < 1000) {
    return;
  }
  _ts = DateTime.now().millisecondsSinceEpoch;
  ftoast.Fluttertoast.showToast(
      msg: msg,
      toastLength: ftoast.Toast.LENGTH_SHORT,
      gravity: ftoast.ToastGravity.CENTER,
      timeInSecForIosWeb: 1,
      backgroundColor: Colors.black.withOpacity(0.5),
      textColor: Colors.white,
      fontSize: 16.0
  );
}