import 'dart:async';
import 'dart:io';
import 'dart:ui' as ui;

import 'package:flutter/cupertino.dart';

class ImageUtils {
  static late ImageStreamListener listener;
  static Future<ui.Image> getImageWidthAndHeight(String url) async {
    ImageStream stream = FileImage(
      File.fromUri(Uri.parse(url)),
    ).resolve(ImageConfiguration.empty);
    Completer<ui.Image> completer = Completer<ui.Image>();
    listener = ImageStreamListener((ImageInfo frame, bool synchronousCall) {
      final ui.Image image = frame.image;
      completer.complete(image);
      stream.removeListener(listener);
    });
    stream.addListener(listener);
    return completer.future;
  }
}
