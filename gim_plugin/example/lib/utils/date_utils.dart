import 'package:intl/intl.dart';

int getCurrentMilliseconds() => DateTime.now().millisecondsSinceEpoch;

DateTime transformMilliseconds2DateTime(int timestamp) {
  return DateTime.fromMillisecondsSinceEpoch(timestamp);
}

DateTime strToDateTime(String str) {
  return DateTime.tryParse(str) ?? DateTime.now();
}

String dateTimeToStr(DateTime dateTime) {
  return dateTime.toString();
}

int getAgeByDateTime(DateTime brt) {
  int age = 0;
  DateTime dateTime = DateTime.now();
  if (dateTime.isBefore(brt)) {
//出生日期晚于当前时间，无法计算
    return 0;
  }
  int yearNow = dateTime.year; //当前年份
  int monthNow = dateTime.month; //当前月份
  int dayOfMonthNow = dateTime.day; //当前日期

  int yearBirth = brt.year;
  int monthBirth = brt.month;
  int dayOfMonthBirth = brt.day;
  age = yearNow - yearBirth; //计算整岁数
  if (monthNow <= monthBirth) {
    if (monthNow == monthBirth) {
      if (dayOfMonthNow < dayOfMonthBirth) age--; //当前日期在生日之前，年龄减一
    } else {
      age--; //当前月份在生日之前，年龄减一
    }
  }
  return age;
}

enum Format { YMD, YMDHMS }

String formatDate(DateTime dateTime, {Format fmt = Format.YMDHMS}) {
  switch (fmt) {
    case Format.YMD:
      return DateFormat('yyyy-MM-dd').format(dateTime);
    case Format.YMDHMS:
      return DateFormat('yyyy-MM-dd hh:mm:ss').format(dateTime);
    default:
      return DateFormat('yyyy-MM-dd hh:mm:ss').format(dateTime);
  }
}

String diffCurrent(DateTime dt, {String prefix = '', String suffix = '', bool mustShow = false}) {
  late Duration diff = DateTime.now().difference(dt);
  if (diff.inSeconds < 60) {
    return "$prefix刚刚$suffix";
  } else if (diff.inMinutes < 60) {
    return "$prefix${diff.inMinutes}分钟前$suffix";
  } else if (diff.inHours < 24) {
    return "$prefix${diff.inHours}小时前$suffix";
  } else if (diff.inHours > 24 && diff.inHours < 48) {
    return "$prefix昨天$suffix";
  } else if (diff.inDays <= 7) {
    return "$prefix${diff.inDays}天前$suffix";
  } else if (mustShow) {
    if (diff.inDays < 365) {
      return "$prefix${DateFormat('MM-dd').format(dt)}$suffix";
    } else {
      return "$prefix${DateFormat('yy-MM-dd').format(dt)}$suffix";
    }
  }
  return "";
}

String homeDiffCurrent(DateTime dt, {String prefix = '', String suffix = ''}) {
  late Duration diff = DateTime.now().difference(dt);
  if (diff.inSeconds < 60) {
    return "$prefix刚刚$suffix";
  } else if (diff.inMinutes < 60) {
    return "$prefix${diff.inMinutes}分钟前$suffix";
  } else if (diff.inHours < 24) {
    return "$prefix${diff.inHours}小时前$suffix";
  } else if (diff.inHours > 24 && diff.inHours < 48) {
    return "$prefix昨天$suffix";
  } else if (diff.inDays <= 7) {
    return "$prefix${diff.inDays}天前$suffix";
  } else {
    return "${prefix}7天前$suffix";
  }
  return "";
}

//毫秒
String getHMS(Duration d) {
  var minutes = d.inMinutes;
  var seconds = d.inSeconds % 60;
// var millSecond = d.inMilliseconds % 1000 ~/ 10;
  var _recorderTxt = "";
  if (minutes > 9) {
    _recorderTxt = _recorderTxt + "$minutes";
  } else {
    _recorderTxt = _recorderTxt + "0$minutes";
  }

  if (seconds > 9) {
    _recorderTxt = _recorderTxt + ":$seconds";
  } else {
    _recorderTxt = _recorderTxt + ":0$seconds";
  }
  return _recorderTxt;
}

///根据日期，返回星座
String getConstellation(DateTime birthday) {
  const capricorn = '摩羯座'; //Capricorn 摩羯座（12月22日～1月20日）
  const aquarius = '水瓶座'; //Aquarius 水瓶座（1月21日～2月19日）
  const pisces = '双鱼座'; //Pisces 双鱼座（2月20日～3月20日）
  const aries = '白羊座'; //3月21日～4月20日
  const taurus = '金牛座'; //4月21～5月21日
  const gemini = '双子座'; //5月22日～6月21日
  const cancer = '巨蟹座'; //Cancer 巨蟹座（6月22日～7月22日）
  const leo = '狮子座'; //Leo 狮子座（7月23日～8月23日）
  const virgo = '处女座'; //Virgo 处女座（8月24日～9月23日）
  const libra = '天秤座'; //Libra 天秤座（9月24日～10月23日）
  const scorpio = '天蝎座'; //Scorpio 天蝎座（10月24日～11月22日）
  const sagittarius = '射手座'; //Sagittarius 射手座（11月23日～12月21日）

  int month = birthday.month;
  int day = birthday.day;
  String constellation = '';

  switch (month) {
    case DateTime.january:
      constellation = day < 21 ? capricorn : aquarius;
      break;
    case DateTime.february:
      constellation = day < 20 ? aquarius : pisces;
      break;
    case DateTime.march:
      constellation = day < 21 ? pisces : aries;
      break;
    case DateTime.april:
      constellation = day < 21 ? aries : taurus;
      break;
    case DateTime.may:
      constellation = day < 22 ? taurus : gemini;
      break;
    case DateTime.june:
      constellation = day < 22 ? gemini : cancer;
      break;
    case DateTime.july:
      constellation = day < 23 ? cancer : leo;
      break;
    case DateTime.august:
      constellation = day < 24 ? leo : virgo;
      break;
    case DateTime.september:
      constellation = day < 24 ? virgo : libra;
      break;
    case DateTime.october:
      constellation = day < 24 ? libra : scorpio;
      break;
    case DateTime.november:
      constellation = day < 23 ? scorpio : sagittarius;
      break;
    case DateTime.december:
      constellation = day < 22 ? sagittarius : capricorn;
      break;
  }

  return constellation;
}
