import 'dart:math';

randomBit(int len) {
  String scopeF = '123456789'; //首位
  String scopeC = '0123456789'; //中间
  String result = '';
  for (int i = 0; i < len; i++) {
    if (i == 0) {
      result = scopeF[Random().nextInt(scopeF.length)];
    } else {
      result = result + scopeC[Random().nextInt(scopeC.length)];
    }
  }
  return result;
}

int randNumber(int max) {
  return Random().nextInt(max);
}

int getRandomInt(int min, int max) {
  final _random = Random();
  return _random.nextInt((max - min).floor()) + min;
}

//打乱数组
shuffle<T>(List<T> arr) {
  List<T> newArr = [];
  newArr.addAll(arr);
  for (var i = 1; i < newArr.length; i++) {
    var j = getRandomInt(0, i);
    var t = newArr[i];
    newArr[i] = newArr[j];
    newArr[j] = t;
  }
  return newArr;
}

int getTotalMember() {
  return 600000 +
      (DateTime.now().difference(DateTime(2022, 1, 1)).inHours * 19.733)
          .toInt();
}

int getTotalMan() {
  return (getTotalMember() * 0.3933).toInt();
}

int getTotalWoman() {
  int total = getTotalMember();
  return total - (total * 0.3933).toInt();
}
