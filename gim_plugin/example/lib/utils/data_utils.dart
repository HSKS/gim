bool isBlack(String? data) => data == null || data.trim() == '';

bool notBlack(String? data) => !isBlack(data);

bool isZero(int? data) => data == null || data == 0;

bool notZero(int? data) => !isZero(data);

int safeInt(int? data) => data ?? 0;

double safeDouble(double? data) => data ?? 0;

String safeString(String? data) => data ?? "";

bool isTrue(dynamic data) =>
    data != null && data != 0 && data != "" && data != false && data != "false";
