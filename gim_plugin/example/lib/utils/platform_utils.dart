import 'dart:io';

import 'package:device_info_plus/device_info_plus.dart';
import './cache.dart';

import 'data_utils.dart';

String? _platform;

Future<String?> getPlatformInfo() async {
  if (notBlack(_platform)) {
    return _platform!;
  }
  _platform = 'ios';
  if (Platform.isAndroid) {
    DeviceInfoPlugin info = DeviceInfoPlugin();
    AndroidDeviceInfo androidInfo = await info.androidInfo;
    String brand = androidInfo.brand!.toLowerCase();
    switch (brand) {
      case 'xiaomi':
        _platform = 'xiaomi';
        break;
      case 'oppo':
        _platform = 'oppo';
        break;
      case 'vivo':
        _platform = 'vivo';
        break;
      case 'huawei':
        _platform = 'huawei';
        break;
      case 'google':
        _platform = 'google';
        break;
      default:
        List<String?> features = androidInfo.systemFeatures;
        if (features.isNotEmpty) {
          String str = features.join('.').toLowerCase();
          if (str.contains('huawei')) {
            _platform = 'huawei';
          } else if (str.contains('vivo')) {
            _platform = 'vivo';
          } else if (str.contains('oppo')) {
            _platform = 'oppo';
          } else if (str.contains('xiaomi')) {
            _platform = 'xiaomi';
          }
        }
    }
  }
  return _platform;
}

Future<String> getDeviceId() async {
  String key = "deviceId";
  String? id = cache.getStringSync(key);
  if (notBlack(id)) {
    return id!;
  }
  DeviceInfoPlugin info = DeviceInfoPlugin();
  if (Platform.isAndroid) {
    id = (await info.androidInfo).id!;
  } else if (Platform.isIOS) {
    id = (await info.iosInfo).identifierForVendor!;
  } else {
    id = "";
  }
  cache.setString(key, id);
  return id;
}
