import 'data_utils.dart';
//手机号验证
bool isPhone(String input) {
  if (input.startsWith("9898") && input.length == 11) return true;
  RegExp mobile = RegExp(r"^1[0-9]\d{9}$");
  return mobile.hasMatch(input);
}

bool isNumber(String? str) {
  if (isBlack(str)) return false;
  RegExp reg = RegExp(r"^[0-9]*$");
  return reg.hasMatch(str!);
}

bool hasZh(String? str) {
  if (isBlack(str)) return false;
  RegExp reg = RegExp(r"[\u4e00-\u9fa5]");
  return reg.hasMatch(str!);
}

//登录密码：6位数字验证码
bool isValidateCaptcha(String input) {
  RegExp mobile = RegExp(r"^\d{6}$");
  return mobile.hasMatch(input);
}

//身份证号码验证
bool isCardId(String cardId) {
  if (cardId.length != 18) {
    return false;
  }
  RegExp postalCode = RegExp(r'^[1-9]\d{5}[1-9]\d{3}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}([0-9]|[Xx])$');
  if (!postalCode.hasMatch(cardId)) {
    return false;
  }
  final List idCardList = ["7", "9", "10", "5", "8", "4", "2", "1", "6", "3", "7", "9", "10", "5", "8", "4", "2"];
  final List idCardYArray = ['1', '0', '10', '9', '8', '7', '6', '5', '4', '3', '2'];
  int idCardWiSum = 0;

  for (int i = 0; i < 17; i++) {
    int subStrIndex = int.parse(cardId.substring(i, i + 1));
    int idCardWiIndex = int.parse(idCardList[i]);
    idCardWiSum += subStrIndex * idCardWiIndex;
  }
  int idCardMod = idCardWiSum % 11;
  String idCardLast = cardId.substring(17, 18);
  if (idCardMod == 2) {
    if (idCardLast != 'x' && idCardLast != 'X') {
      return false;
    }
  } else {
    if (idCardLast != idCardYArray[idCardMod]) {
      return false;
    }
  }
  return true;
}
