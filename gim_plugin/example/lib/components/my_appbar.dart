import 'package:flutter/material.dart';

import '../std.dart';

AppBar myAppBar(String title,
    {Key? key,
    Widget? leading,
    double? leadingWidth,
    double elevation = 0,
    PreferredSizeWidget? bottom,
    bool bottomLine = false,
    List<Widget>? actions}) {
  return AppBar(
    key: key,
    iconTheme: IconThemeData(color: blackColor),
    backgroundColor: Colors.white,
    centerTitle: true,
    title: pgTitle(title),
    leading: leading,
    leadingWidth: leadingWidth,
    elevation: elevation,
    bottom: bottomLine
        ? PreferredSize(
            preferredSize: const Size.fromHeight(0.5),
            child: Divider(color: Colors.black.withOpacity(0.05), height: 0.5),
          )
        : bottom,
    actions: actions,
  );
}
