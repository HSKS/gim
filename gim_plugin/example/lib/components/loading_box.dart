import 'package:flutter/material.dart';

class LoadingBox extends StatelessWidget {
  Widget child;

  Widget? loadingIcon;

  double? width;

  double? height;

  LoadingBox(this.child, {Key? key, this.loadingIcon, this.width, this.height}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: width,
      height: height,
      child: Stack(
        alignment: Alignment.center,
        children: [
          child,
          loadingIcon != null ? loadingIcon! : CircularProgressIndicator(),
        ],
      ),
    );
  }
}
