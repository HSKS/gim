import 'package:flutter/cupertino.dart';

class MyBtn extends StatelessWidget {
  final double width;
  final double height;
  final Color? color;
  final double borderRadius;
  final Gradient? gradient;
  final List<BoxShadow>? boxShadow;
  final BoxFit? boxFit;
  final String? bgImage;
  final VoidCallback onClick;
  final Widget child;

  MyBtn(
      {Key? key,
      required this.width,
      required this.height,
      required this.onClick,
      this.color,
      this.borderRadius = 0,
      this.gradient,
      this.boxShadow,
      this.boxFit,
      this.bgImage,
        required this.child
      })
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CupertinoButton(
        padding: EdgeInsets.zero,
        child: Container(
          width: width,
          height: height,
          alignment: Alignment.center,
          decoration: BoxDecoration(
              color: color,
              borderRadius: BorderRadius.circular(borderRadius),
              gradient: gradient,
              boxShadow: boxShadow),
          child: child,
        ),
        onPressed: onClick);
  }
}
