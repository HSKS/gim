import 'package:gim_plugin_example/std.dart';
import 'package:flutter/material.dart';

Widget inputBox(
    {required TextEditingController controller,
    required String placeholder,
    ValueChanged<String>? onSubmitted,
    ValueChanged<String>? onChange,
    bool obscureText = false,
    int maxLength = 11,
    TextInputType? keyboardType,
    String? errText,
    TextInputAction? textInputAction}) {
  return TextField(
    controller: controller,
    maxLength: maxLength,
    autocorrect: true,
    obscureText: obscureText,
    textAlign: TextAlign.start,
    keyboardType: keyboardType,
    textInputAction: textInputAction,
    decoration: InputDecoration(
      hintText: placeholder,
      errorText: errText,
      hintStyle: TextStyle(
        fontSize: font20,
        fontWeight: FontWeight.w400,
        color: greyColor,
      ),
    ),
    style: TextStyle(
      fontSize: font20,
      fontWeight: FontWeight.w400,
      color: Colors.black,
    ),
    onSubmitted: onSubmitted,
    onChanged: onChange,
  );
}
