import 'package:flutter/material.dart';
import 'package:photo_view/photo_view_gallery.dart';

import '../config.dart';
import '../std.dart';

class _PhotoViewModal extends StatefulWidget {
  final List<String> photos;
  final int initIndex;

  _PhotoViewModal({required this.photos, this.initIndex = 0});

  @override
  State<StatefulWidget> createState() => _PhotoViewModalState();
}

class _PhotoViewModalState extends State<_PhotoViewModal> {
  late PageController controller;

  late int currPage;

  @override
  void initState() {
    controller = PageController(initialPage: widget.initIndex);
    currPage = widget.initIndex;
    super.initState();
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.bottomCenter,
      children: [
        Container(
          width: windowWidth,
          height: windowHeight,
          color: const Color(0xFF171A20),
          child: PhotoViewGallery.builder(
            pageController: controller,
            itemCount: widget.photos.length,
            onPageChanged: (i) => setState(() {
              currPage = i;
            }),
            builder: (BuildContext context, int index) {
              return PhotoViewGalleryPageOptions.customChild(
                  child: Container(
                width: windowWidth,
                height: windowHeight,
                decoration: const BoxDecoration(color: Color(0xFF171A20)),
                child: Image.network(genCosUrl(widget.photos[index]),
                    width: windowWidth, fit: BoxFit.contain),
              ));
            },
            loadingBuilder: (context, event) => Center(
              child: SizedBox(
                width: 20.0,
                height: 20.0,
                child: CircularProgressIndicator(
                  color: themeColor,
                  backgroundColor: const Color.fromRGBO(34, 34, 34, 0.15),
                  value: event == null ? 0 : event.cumulativeBytesLoaded / event.expectedTotalBytes!,
                ),
              ),
            ),
          ),
        ),
        Positioned(
            bottom: bottomSafeHeight + 30,
            child: Material(
              color: const Color.fromRGBO(0, 0, 0, 0.5),
              borderRadius: BorderRadius.circular(8),
              child: Padding(
                padding: const EdgeInsets.only(top: 4, bottom: 4, left: 8, right: 8),
                child: text16('${currPage + 1}/${widget.photos.length}',
                    color: Colors.white, fontWeight: FontWeight.w600),
              ),
            ))
      ],
    );
  }
}

class _PhotoViewModalOverlay {
  OverlayEntry? _entry;

  void open(BuildContext context, {required List<String> photos, int initIndex = 0}) {
    _entry = OverlayEntry(builder: (_) {
      return GestureDetector(
        behavior: HitTestBehavior.opaque,
        onTap: () {
          close();
        },
        child: _PhotoViewModal(photos: photos, initIndex: initIndex),
      );
      // return Stack(
      //   children: [
      //     _HiPhotoView(photos: photos, initIndex: initIndex),
      //     Positioned(
      //         top: topSafeHeight + 20,
      //         right: 20,
      //         width: 30,
      //         height: 30,
      //         child: CupertinoButton(
      //           padding: EdgeInsets.zero,
      //             color: const Color.fromRGBO(255, 255, 255, 0.5),
      //             borderRadius: BorderRadius.circular(15),
      //             child: const Icon(Icons.close, color: Colors.white, size: 20),
      //             onPressed: () {
      //               close();
      //             }))
      //   ],
      // );
    });
    Overlay.of(context)!.insert(_entry!);
  }

  void close() {
    _entry?.remove();
  }
}

_PhotoViewModalOverlay photoViewModal = _PhotoViewModalOverlay();
