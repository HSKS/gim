import 'package:flutter/material.dart';
import 'package:loading_animation_widget/loading_animation_widget.dart';

class _LoadingModal {
  OverlayEntry? _entry;
  void open(BuildContext context) {
    _entry = OverlayEntry(builder: (BuildContext context) {
      return Container(
          width: double.infinity,
          height: double.infinity,
          alignment: Alignment.center,
          decoration: const BoxDecoration(
              color: Color.fromRGBO(0, 0, 0, 0.2)
          ),
          child: Container(
            width: 120,
            height: 120,
            decoration: BoxDecoration(
              color: Colors.black.withOpacity(0.4),
              borderRadius: BorderRadius.circular(20)
            ),

            child: LoadingAnimationWidget.fallingDot(color: Colors.white, size: 50),
          ));
    });
    Overlay.of(context)!.insert(_entry!);
  }
  void close() {
    if (_entry != null) {
      Future.delayed(const Duration(milliseconds: 30)).then((value) {
        _entry?.remove();
      });
    }
  }
}

_LoadingModal loadingModal = _LoadingModal();
