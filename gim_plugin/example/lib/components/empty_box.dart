import 'package:empty_widget/empty_widget.dart';
import 'package:flutter/material.dart';

import '../std.dart';

class EmptyBox extends StatelessWidget {
  final double size;
  final String tip;

  EmptyBox({this.size = 200, this.tip = '暂无数据'});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: windowWidth,
      height: windowHeight * 0.6,
      alignment: Alignment.center,
      child: SizedBox(
        width: size,
        height: size,
        child: EmptyWidget(
          packageImage: PackageImage.Image_4,
          title: null,
          subTitle: tip,
          titleTextStyle: const TextStyle(
            fontSize: 22,
            color: Color(0xff9da9c7),
            fontWeight: FontWeight.w500,
          ),
          subtitleTextStyle: const TextStyle(
            fontSize: 14,
            color: Color(0xffabb8d6),
          ),
          // Uncomment below statement to hide background animation
          // hideBackgroundAnimation: true,
        ),
      ),
    );
  }

}