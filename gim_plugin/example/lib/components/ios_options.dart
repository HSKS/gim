import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../std.dart';

class IosOptionItem {
  final String label;
  final VoidCallback onTap;

  IosOptionItem({required this.label, required this.onTap});
}

class IosOptions {
  static bool bottomOptionsExist = false;

  static Future<void> showBottomOptions(BuildContext context,
      {VoidCallback? onCancel, String? title, required List<IosOptionItem> items}) async {
    if (bottomOptionsExist) return;
    bottomOptionsExist = true;
    await showCupertinoModalPopup(
        context: context,
        builder: (_) {
          return CupertinoActionSheet(
            title: title != null ? Text(title, style: const TextStyle(fontSize: 16)) : null,
            cancelButton: CupertinoButton(
                child: Text("取消", style: TextStyle(color: greyColor)),
                onPressed: () {
                  Navigator.of(context).pop();
                  if (onCancel != null) onCancel();
                }),
            actions: items
                .map((item) => CupertinoActionSheetAction(
                      child: Text(item.label, style: TextStyle(color: themeColor)),
                      onPressed: () {
                        Navigator.of(context).pop();
                        Future.delayed(Duration.zero).then((_) => item.onTap());
                      },
                    ))
                .toList(),
          );
        });
    bottomOptionsExist = false;
  }

  static bool confirmModalExist = false;

  static Future<void> showConfirmModal(BuildContext context,
      {String? title, Widget? content, VoidCallback? onCancel, List<IosOptionItem>? items}) async {
    if (confirmModalExist) return;
    confirmModalExist = true;
    List<Widget> opts = [
      CupertinoButton(
          child: const Text("取消", style: TextStyle(color: Colors.red)),
          onPressed: () {
            Navigator.of(context).pop();
            if (onCancel != null) onCancel();
          })
    ];
    if (items != null && items.isNotEmpty) {
      for (var item in items) {
        opts.add(CupertinoButton(
            child: Text(item.label, style: const TextStyle(color: Colors.blue)),
            onPressed: () {
              Navigator.of(context).pop();
              item.onTap();
            }));
      }
    }
    await showCupertinoDialog(
        context: context,
        builder: (_) {
          return CupertinoAlertDialog(
            title: title != null ? Text(title) : null,
            content: content,
            actions: opts,
          );
        });
    confirmModalExist = false;
  }

  static bool centerModalExist = false;

  static Future<void> showCenterModal(BuildContext context,
      {required String title,
      Widget? child,
      String? cancelText,
      String? okText,
      VoidCallback? onCancel,
      VoidCallback? onOk}) async {
    if (centerModalExist) return;
    centerModalExist = true;
    cancelText ??= '取消';
    double btnWidth = windowWidth * 0.23;
    double btnHeight = 48;
    List<Widget> opts = [
      CupertinoButton(
          padding: EdgeInsets.zero,
          child: Container(
            width: btnWidth,
            height: btnHeight,
            alignment: Alignment.center,
            decoration: BoxDecoration(
                border: Border.all(color: Colors.black, width: 3), borderRadius: BorderRadius.circular(btnHeight / 2)),
            child: text16(cancelText, fontWeight: FontWeight.w400, color: Colors.black),
          ),
          onPressed: () {
            Navigator.of(context).pop();
            if (onCancel != null) onCancel();
          }),
    ];
    if (okText != null && okText != '' && onOk != null) {
      opts.add(CupertinoButton(
          padding: EdgeInsets.zero,
          child: Container(
            width: btnWidth,
            height: btnHeight,
            alignment: Alignment.center,
            decoration: BoxDecoration(
                color: Colors.black,
                border: Border.all(color: Colors.black, width: 3),
                borderRadius: BorderRadius.circular(btnHeight / 2)),
            child: text16(okText, fontWeight: FontWeight.w400, color: Colors.white),
          ),
          onPressed: () {
            Navigator.of(context).pop();
            onOk();
          }));
    }
    await showCupertinoDialog(
        context: context,
        builder: (_) {
          return Material(
            color: const Color.fromRGBO(0, 0, 0, 0.3),
            child: Center(
              child: Container(
                width: windowWidth * 0.733,
                padding: const EdgeInsets.all(20),
                decoration: BoxDecoration(borderRadius: BorderRadius.circular(15), color: Colors.white),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    text18(title, fontWeight: FontWeight.w600),
                    Padding(padding: const EdgeInsets.only(top: 20, bottom: 20), child: child),
                    Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: opts,
                    )
                  ],
                ),
              ),
            ),
          );
        });
    centerModalExist = false;
  }

  static Future<void> showBottomModal(
    BuildContext context, {
    required Widget child,
    Color? color,
    bool isDismissible = true,
    bool enableDrag = true,
  }) async {
    await showModalBottomSheet(
        context: context,
        isDismissible: isDismissible,
        enableDrag: enableDrag,
        isScrollControlled: true,
        backgroundColor: Colors.transparent,
        builder: (_) {
          return Scaffold(
              backgroundColor: Colors.transparent,
              body: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Container(
                    width: windowWidth,
                    decoration: BoxDecoration(
                        color: color,
                        borderRadius:
                            const BorderRadius.only(topLeft: Radius.circular(10), topRight: Radius.circular(10))),
                    child: child,
                  )
                ],
              ));
        });
  }
}
