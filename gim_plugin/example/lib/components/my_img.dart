import 'dart:io';

import 'package:flutter/material.dart';

import '../config.dart';
import '../consts.dart';

Widget myImg(String url, {double? width, double? height, BoxFit fit = BoxFit.cover}) {
  Widget cld;
  if (url.startsWith("http") || url.startsWith(ASSET_PREFIX)) {
    cld = Image.network(genCosUrl(url), width: width, height: height, fit: fit);
  } else if (url.startsWith("images/") || url.startsWith("icons/")) {
    cld = Image.asset(url, width: width, height: height, fit: fit);
  } else {
    cld = Image.file(File(url), width: width, height: height, fit: fit);
  }
  return Container(
    width: width,
    height: height,
    decoration: BoxDecoration(
      color: Colors.grey[200],
    ),
    child: cld,
  );
}

ImageProvider imgProvider(String url) {
  if (url.startsWith("http") || url.startsWith(ASSET_PREFIX)) {
    return NetworkImage(genCosUrl(url));
  } else if (url.startsWith("images/") || url.startsWith("icons/")) {
    return AssetImage(url);
  } else {
    return FileImage(File(url));
  }
}
