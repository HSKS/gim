import 'package:flutter/material.dart';
import 'dart:ui' as ui show window;
import 'package:flutter/services.dart';

double get pgPadding => 20;

double get font24 => 24;

double get font22 => 22;

double get font20 => 20;

double get font18 => 18;

double get font16 => 16;

double get font15 => 15;

double get font14 => 14;

double get font12 => 12;

double get font11 => 11;

double get font10 => 10;

Color get bgColor => const Color(0xFFFAFAFA);

Color get themeColor => const Color(0xFFDE9E46);

Color get blackColor => const Color(0xFF000000);

Color get greyColor => const Color(0xFF808A8B);

Color get redColor => const Color(0xFF995E0D);

Color get blueColor => const Color(0xFF4593FF);

double get windowWidth {
  MediaQueryData mediaQuery = MediaQueryData.fromWindow(ui.window);
  return mediaQuery.size.width;
}

double get windowHeight {
  MediaQueryData mediaQuery = MediaQueryData.fromWindow(ui.window);
  return mediaQuery.size.height;
}

double get windowScale {
  MediaQueryData mediaQuery = MediaQueryData.fromWindow(ui.window);
  return mediaQuery.devicePixelRatio;
}

double get textScaleFactor {
  MediaQueryData mediaQuery = MediaQueryData.fromWindow(ui.window);
  return mediaQuery.textScaleFactor;
}

double get navigationBarHeight {
  MediaQueryData mediaQuery = MediaQueryData.fromWindow(ui.window);
  return mediaQuery.padding.top + kToolbarHeight;
}

double get topSafeHeight {
  MediaQueryData mediaQuery = MediaQueryData.fromWindow(ui.window);
  return mediaQuery.padding.top;
}

double get bottomSafeHeight {
  MediaQueryData mediaQuery = MediaQueryData.fromWindow(ui.window);
  return mediaQuery.padding.bottom;
}

updateStatusBarStyle(SystemUiOverlayStyle style) {
  SystemChrome.setSystemUIOverlayStyle(style);
}

const gradient = LinearGradient(
//渐变位置
    begin: Alignment.centerLeft, //右上
    end: Alignment.centerRight, //左下
    stops: [0.0, 1.0], //[渐变起始点, 渐变结束点]
//渐变颜色[始点颜色, 结束颜色]
    colors: [Color(0xFFFEDBA6), Color(0xFFF5C17B)]);

Text textGradient(String data,
    {double? height,
      double? fontSize,
      TextAlign? textAlign,
      Gradient gradient = gradient,
      FontWeight fontWeight = FontWeight.w400,
      double? letterSpacing,
      double? wordSpacing}) {
  fontSize ??= font14;
  return Text(data,
      textAlign: textAlign,
      style: TextStyle(
          height: height,
          fontSize: fontSize,
          fontWeight: fontWeight,
          letterSpacing: letterSpacing,
          wordSpacing: wordSpacing,
          foreground: Paint()
            ..style = PaintingStyle.fill
            ..strokeWidth = 10
            ..shader = gradient.createShader(Rect.zero)));
}

Text textDot(String data,
    {int maxLines = 1,
      double? fontSize,
      Color? color,
      double? height,
      TextAlign? textAlign,
      FontWeight fontWeight = FontWeight.w400,
      double? letterSpacing,
      double? wordSpacing}) {
  fontSize ??= font14;
  color ??= blackColor;
  return Text(
    data,
    textAlign: textAlign,
    overflow: TextOverflow.ellipsis,
    maxLines: maxLines,
    style: TextStyle(
        color: color,
        height: height,
        fontSize: fontSize,
        fontWeight: fontWeight,
        letterSpacing: letterSpacing,
        wordSpacing: wordSpacing),
  );
}

Text pgTitle(String data, {double? fontSize, Color? color}) {
  color ??= blackColor;
  return Text(data, style: TextStyle(fontSize: fontSize, color: color, fontWeight: FontWeight.w600));
}

Text text24(String data,
    {Color? color,
      double? height,
      TextAlign? textAlign,
      FontWeight fontWeight = FontWeight.w400,
      double? letterSpacing,
      double? wordSpacing}) {
  color ??= blackColor;
  return Text(data,
      textAlign: textAlign,
      style: TextStyle(
          color: color,
          height: height,
          fontSize: font24,
          fontWeight: fontWeight,
          letterSpacing: letterSpacing,
          wordSpacing: wordSpacing));
}

Text text22(String data,
    {Color? color,
      double? height,
      TextAlign? textAlign,
      FontWeight fontWeight = FontWeight.w400,
      double? letterSpacing,
      double? wordSpacing}) {
  color ??= blackColor;
  return Text(data,
      textAlign: textAlign,
      style: TextStyle(
          color: color,
          height: height,
          fontSize: font22,
          fontWeight: fontWeight,
          letterSpacing: letterSpacing,
          wordSpacing: wordSpacing));
}

Text text20(String data,
    {Color? color,
      double? height,
      TextAlign? textAlign,
      FontWeight fontWeight = FontWeight.w400,
      double? letterSpacing,
      double? wordSpacing}) {
  color ??= blackColor;
  return Text(data,
      textAlign: textAlign,
      style: TextStyle(
          color: color,
          height: height,
          fontSize: font20,
          fontWeight: fontWeight,
          letterSpacing: letterSpacing,
          wordSpacing: wordSpacing));
}

Text text18(String data,
    {Color? color,
      double? height,
      TextAlign? textAlign,
      FontWeight fontWeight = FontWeight.w400,
      double? letterSpacing,
      double? wordSpacing}) {
  color ??= blackColor;
  return Text(data,
      textAlign: textAlign,
      style: TextStyle(
          color: color,
          height: height,
          fontSize: font18,
          fontWeight: fontWeight,
          letterSpacing: letterSpacing,
          wordSpacing: wordSpacing));
}

Text text16(String data,
    {Color? color,
      double? height,
      TextAlign? textAlign,
      FontWeight fontWeight = FontWeight.w400,
      double? letterSpacing,
      double? wordSpacing}) {
  color ??= blackColor;
  return Text(data,
      textAlign: textAlign,
      style: TextStyle(
          color: color,
          height: height,
          fontSize: font16,
          fontWeight: fontWeight,
          letterSpacing: letterSpacing,
          wordSpacing: wordSpacing));
}

Text text15(String data,
    {Color? color,
      double? height,
      TextAlign? textAlign,
      FontWeight fontWeight = FontWeight.w400,
      double? letterSpacing,
      double? wordSpacing}) {
  color ??= blackColor;
  return Text(data,
      textAlign: textAlign,
      style: TextStyle(
          color: color,
          height: height,
          fontSize: font15,
          fontWeight: fontWeight,
          letterSpacing: letterSpacing,
          wordSpacing: wordSpacing));
}

Text text14(String data,
    {Color? color,
      double? height,
      TextAlign? textAlign,
      FontWeight fontWeight = FontWeight.w400,
      double? letterSpacing,
      double? wordSpacing}) {
  color ??= blackColor;
  return Text(data,
      textAlign: textAlign,
      style: TextStyle(
          color: color,
          height: height,
          fontSize: font14,
          fontWeight: fontWeight,
          letterSpacing: letterSpacing,
          wordSpacing: wordSpacing));
}

Text text13(String data,
    {Color? color,
      double? height,
      TextAlign? textAlign,
      FontWeight fontWeight = FontWeight.w400,
      double? letterSpacing,
      double? wordSpacing}) {
  color ??= blackColor;
  return Text(data,
      textAlign: textAlign,
      style: TextStyle(
          color: color,
          height: height,
          fontSize: 13,
          fontWeight: fontWeight,
          letterSpacing: letterSpacing,
          wordSpacing: wordSpacing));
}

Text text12(String data,
    {Color? color,
      double? height,
      TextAlign? textAlign,
      FontWeight fontWeight = FontWeight.w400,
      double? letterSpacing,
      double? wordSpacing}) {
  color ??= blackColor;
  return Text(data,
      textAlign: textAlign,
      style: TextStyle(
          color: color,
          height: height,
          fontSize: font12,
          fontWeight: fontWeight,
          letterSpacing: letterSpacing,
          wordSpacing: wordSpacing));
}

Text text11(String data,
    {Color? color,
      double? height,
      TextAlign? textAlign,
      FontWeight fontWeight = FontWeight.w400,
      double? letterSpacing,
      double? wordSpacing}) {
  color ??= blackColor;
  return Text(data,
      textAlign: textAlign,
      style: TextStyle(
          color: color,
          height: height,
          fontSize: font11,
          fontWeight: fontWeight,
          letterSpacing: letterSpacing,
          wordSpacing: wordSpacing));
}

Text text10(String data,
    {Color? color,
      double? height,
      TextAlign? textAlign,
      FontWeight fontWeight = FontWeight.w400,
      double? letterSpacing,
      double? wordSpacing}) {
  color ??= blackColor;
  return Text(data,
      textAlign: textAlign,
      style: TextStyle(
          color: color,
          height: height,
          fontSize: font10,
          fontWeight: fontWeight,
          letterSpacing: letterSpacing,
          wordSpacing: wordSpacing));
}
