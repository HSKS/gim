import 'package:gim_plugin_example/service/gim_podo/GetSignKeyDto.dart';
import 'package:gim_plugin_example/utils/ajax.dart';

class GimSvc {
  static Future<GetSignKeyDto?> getSignKey({OnError? onError}) async {
    var api = Api('/gim/trtc/getSignKey', ApiMethod.post);
    var dto = await ajax.request(api, onError: onError);
    if (dto == null) return null;
    return GetSignKeyDto.fromJson(dto);
  }
}
