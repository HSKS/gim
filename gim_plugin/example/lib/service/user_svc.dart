import 'package:gim_plugin_example/service/user_podo/GetConversationInfosDto.dart';
import 'package:gim_plugin_example/service/user_podo/GetUserListDto.dart';
import 'package:gim_plugin_example/service/user_podo/UserInfo.dart';
import 'package:gim_plugin_example/service/user_podo/UserLoginDto.dart';
import 'package:gim_plugin_example/utils/ajax.dart';
import 'package:gim_plugin_example/utils/cache.dart';
import 'package:gim_plugin_example/utils/logger_utils.dart';

import '../consts.dart';

class UserSvc {

  static Future<UserLoginDto?> userLogin({required String uid, OnError? onError}) async {
    var api = Api('/gim/demo/userLogin', ApiMethod.post);
    var dto = await ajax.request(api,
        data: {
          "uid": uid,
        },
        onError: onError);
    if (dto == null) return null;
    var info = UserLoginDto.fromJson(dto);
    cache.setString(TOKEN, info.token!);
    loggerDebug(info.token);
    cache.setString(UID, info.uid!);
    cache.setString(NICKNAME, info.nickname!);
    cache.setString(AVATAR, info.avatar!);
    return info;
  }

  static Future<UserInfo?> editProfile({String? nickname, avatar, int? gender, int? regStep, OnError? onError}) async {
    var api = Api('/gim/demo/editProfile', ApiMethod.post);
    var dto = await ajax.request(api,
        data: {
          "nickname": nickname,
          "avatar": avatar,
          "gender": gender,
          "reg_step": regStep,
        },
        onError: onError);
    if (dto == null) return null;
    return UserInfo.fromJson(dto);
  }

  static Future<GetUserListDto?> getUserList({int? pageNo, int? pageSize, OnError? onError}) async {
    var api = Api('/gim/demo/getUserList', ApiMethod.post);
    var dto = await ajax.request(api,
        data: {
          "page_no": pageNo,
          "page_size": pageSize,
        },
        onError: onError);
    if (dto == null) return null;
    return GetUserListDto.fromJson(dto);
  }

  static Future<GetConversationInfosDto?> getConversationInfos({required List<String> uids, OnError? onError}) async {
    var api = Api('/gim/demo/getConversationInfos', ApiMethod.post);
    var dto = await ajax.request(api,
        data: {
          "uids": uids,
        },
        onError: onError);
    if (dto == null) return null;
    return GetConversationInfosDto.fromJson(dto);
  }
}
