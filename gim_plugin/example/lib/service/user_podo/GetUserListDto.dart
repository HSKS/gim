import 'UserInfo.dart';

/// arr : [{"age":0}]
/// page_no : 0
/// page_size : 0

class GetUserListDto {
  GetUserListDto({
      this.arr, 
      this.pageNo, 
      this.pageSize,});

  GetUserListDto.fromJson(dynamic json) {
    if (json['arr'] != null) {
      arr = [];
      json['arr'].forEach((v) {
        arr?.add(UserInfo.fromJson(v));
      });
    }
    pageNo = json['page_no'];
    pageSize = json['page_size'];
  }
  List<UserInfo>? arr;
  int? pageNo;
  int? pageSize;
GetUserListDto copyWith({  List<UserInfo>? arr,
  int? pageNo,
  int? pageSize,
}) => GetUserListDto(  arr: arr ?? this.arr,
  pageNo: pageNo ?? this.pageNo,
  pageSize: pageSize ?? this.pageSize,
);
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (arr != null) {
      map['arr'] = arr?.map((v) => v.toJson()).toList();
    }
    map['page_no'] = pageNo;
    map['page_size'] = pageSize;
    return map;
  }

}
