/// id : 0
/// uid : ""
/// nickname : ""
/// avatar : ""
/// gender : 0
/// reg_step : 0
/// created_at : 0
/// last_active : 0

class UserInfo {
  UserInfo({
    this.id,
    this.uid,
    this.nickname,
    this.avatar,
    this.gender,
    this.regStep,
    this.createdAt,
    this.lastActive,
  });

  UserInfo.fromJson(dynamic json) {
    id = json['id'];
    uid = json['uid'];
    nickname = json['nickname'];
    avatar = json['avatar'];
    gender = json['gender'];
    regStep = json['reg_step'];
    createdAt = json['created_at'];
    lastActive = json['last_active'];
  }

  int? id;
  String? uid;
  String? nickname;
  String? avatar;
  int? gender;
  int? regStep;
  int? createdAt;
  int? lastActive;

  UserInfo copyWith({
    int? id,
    String? uid,
    String? nickname,
    String? avatar,
    int? gender,
    int? regStep,
    int? createdAt,
    int? lastActive,
  }) =>
      UserInfo(
        id: id ?? this.id,
        uid: uid ?? this.uid,
        nickname: nickname ?? this.nickname,
        avatar: avatar ?? this.avatar,
        gender: gender ?? this.gender,
        regStep: regStep ?? this.regStep,
        createdAt: createdAt ?? this.createdAt,
        lastActive: lastActive ?? this.lastActive,
      );

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = id;
    map['uid'] = uid;
    map['nickname'] = nickname;
    map['avatar'] = avatar;
    map['gender'] = gender;
    map['reg_step'] = regStep;
    map['created_at'] = createdAt;
    map['last_active'] = lastActive;
    return map;
  }
}
