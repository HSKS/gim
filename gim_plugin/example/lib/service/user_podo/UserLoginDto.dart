/// token : ""
/// uid : ""
/// nickname : ""
/// avatar : ""
/// gender : 0
/// reg_step : 0

class UserLoginDto {
  UserLoginDto({
    this.token,
    this.uid,
    this.nickname,
    this.avatar,
    this.gender,
    this.regStep,
  });

  UserLoginDto.fromJson(dynamic json) {
    token = json['token'];
    uid = json['uid'];
    nickname = json['nickname'];
    avatar = json['avatar'];
    gender = json['gender'];
    regStep = json['reg_step'];
  }

  String? token;
  String? uid;
  String? nickname;
  String? avatar;
  int? gender;
  int? regStep;

  UserLoginDto copyWith({
    String? token,
    String? uid,
    String? nickname,
    String? avatar,
    int? gender,
    int? regStep,
  }) =>
      UserLoginDto(
        token: token ?? this.token,
        uid: uid ?? this.uid,
        nickname: nickname ?? this.nickname,
        avatar: avatar ?? this.avatar,
        gender: gender ?? this.gender,
        regStep: regStep ?? this.regStep,
      );

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['token'] = token;
    map['uid'] = uid;
    map['nickname'] = nickname;
    map['avatar'] = avatar;
    map['gender'] = gender;
    map['reg_step'] = regStep;
    return map;
  }
}
