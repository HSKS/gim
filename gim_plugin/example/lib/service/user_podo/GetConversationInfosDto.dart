import 'package:gim_plugin_example/service/user_podo/UserInfo.dart';

class GetConversationInfosDto {
  Map<String, UserInfo>? userInfos;

  GetConversationInfosDto.fromJson(dynamic json) {
    userInfos = {};
    if (json['user_infos'] != null) {
      json['user_infos'].forEach((key, value) {
        userInfos![key] = UserInfo.fromJson(value);
      });
    }
  }
}
