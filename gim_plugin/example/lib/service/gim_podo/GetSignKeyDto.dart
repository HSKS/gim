/// app_id : 0
/// sign_key : ""
/// uid : ""

class GetSignKeyDto {
  GetSignKeyDto({
    this.appId,
    this.signKey,
    this.uid,
  });

  GetSignKeyDto.fromJson(dynamic json) {
    appId = json['app_id'];
    signKey = json['sign_key'];
    uid = json['uid'];
  }

  int? appId;
  String? signKey;
  String? uid;

  GetSignKeyDto copyWith({
    int? appId,
    String? signKey,
    String? uid,
  }) =>
      GetSignKeyDto(
        appId: appId ?? this.appId,
        signKey: signKey ?? this.signKey,
        uid: uid ?? this.uid,
      );

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['app_id'] = appId;
    map['sign_key'] = signKey;
    map['uid'] = uid;
    return map;
  }
}
