import 'package:gim_plugin/gim_plugin.dart';

class _GimSdk {
  late Gim gim;

  void init(
      {bool ssl = true,
      bool debug = true,
      required String uid,
      required String domain,
      required String appId,
      required String platform,
      required String token}) async {
    gim = Gim(ssl: ssl, debug: debug, uid: uid, domain: domain, appId: appId, platform: platform, token: token);
  }
}

_GimSdk gimSdk = _GimSdk();
